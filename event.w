<?xml version="1.0" encoding="utf-8"?>

<div xmlns="http://www.w3.org/1999/xhtml" xid="window" class="window container-fluid" component="$UI/system/components/justep/window/window"
  design="device:m;">  
  <div component="$UI/system/components/justep/model/model" xid="model" style="height:auto;top:335px;left:508px;"
    onLoad="modelLoad"> 
    <div component="$UI/system/components/justep/data/data" autoLoad="true"
      xid="subdata" idColumn="f_SubID"> 
      <column label="" name="f_SubID" type="String" xid="xid1"/>  
      <column label="" name="f_SubName" type="String" xid="xid2"/>  
      <data xid="default1">[]</data>
    </div>  
    <div component="$UI/system/components/justep/data/data" autoLoad="true"
      xid="eventdata" idColumn="starttime"> 
      <column name="starttime" type="String" xid="xid5"/>  
      <column name="endtime" type="String" xid="xid6"/>  
      <column name="alarmtype" type="String" xid="xid7"/>  
      <column name="metername" type="String" xid="xid8"/>  
      <column name="paramname" type="String" xid="xid9"/>  
      <column name="value" type="String" xid="xid10"/>  
      <column name="limitvalue" type="String" xid="xid11"/>  
      <column name="alarmdesc" type="String" xid="xid12"/>  
      <data xid="default4">[]</data> 
    </div>  
    <div component="$UI/system/components/justep/data/data" autoLoad="true"
      xid="eventtypedata" idColumn="eventtype"> 
      <column name="eventtype" type="String" xid="xid3"/>  
      <column name="typename" type="String" xid="xid4"/>  
      <data xid="default2">[{"eventtype":"1","typename":"越限事件"},{"eventtype":"2","typename":"遥信事件"}]</data> 
    </div> 
  </div>  
  
  
  <html lang="zh-CN"> 
    <head> 
      <meta charset="utf-8"/>  
      <meta http-equiv="X-UA-Compatible" content="IE=edge"/>  
      <meta name="viewport" content="width=device-width, initial-scale=1"/>  
      <title>变电所运维云平台</title>  
      <link href="css/bootstrap.min.css" rel="stylesheet"/>  
      <link href="css/main.css" rel="stylesheet"/> 
    </head> 
  </html>  
  <div xid="div5" bind-click="div5Click"> 
    <span component="$UI/system/components/justep/windowDialog/windowDialog"
      xid="windowDialog1" src="$UI/app/signalevent.w" status="normal" forceRefreshOnOpen="false"
      width="80%" height="auto" style="top:224px;left:783px;" showTitle="true" title="遥信事件详情"/>  
    <span component="$UI/system/components/justep/windowDialog/windowDialog"
      xid="windowDialog2" src="$UI/app/overlimitevent.w" status="normal" height="auto"
      style="top:180px;left:782px;" forceRefreshOnOpen="false" showTitle="true" title="越限事件详情"/> 
  </div>  
  <div component="$UI/system/components/justep/wing/wing" class="x-wing" xid="wing1"> 
    <div class="x-wing-left" xid="left1" style="background-color:#EDEEEE;border-right-style:solid;border-right-width:1px;border-color:#898989"> 
      <div xid="div10" class="pull-left" style="position:absolute;"> 
        <div xid="div4"> 
          <img src="./image/select.jpg" alt="" xid="image1" dir="rtl" style="width:100%;"/> 
        </div>  
        <div component="$UI/system/components/justep/labelEdit/labelEdit" class="x-label-edit x-label30"
          xid="labelEdit7" style="margin-top:5%;width:90%;margin-left:5%;"> 
          <label class="x-label" xid="label6">事件类型</label>  
          <select component="$UI/system/components/justep/select/select" class="form-control x-edit"
            xid="eventtype" bind-options="eventtypedata" bind-optionsValue="eventtype"
            bind-optionsLabel="typename"/> 
        </div>  
        <div component="$UI/system/components/justep/labelEdit/labelEdit" class="x-label-edit x-label30"
          xid="labelEdit3" style="width:90%;margin-left:5%;"> 
          <label class="x-label" xid="label2">变配电站</label>  
          <select component="$UI/system/components/justep/select/select" class="form-control x-edit"
            xid="subid" bind-options="subdata" bind-optionsValue="f_SubID" bind-optionsLabel="f_SubName"/> 
        </div>  
        <div component="$UI/system/components/justep/labelEdit/labelEdit" class="x-label-edit x-label30 pull-left"
          xid="labelEdit6" style="left:10%;width:90%;margin-left:5%;"> 
          <label class="x-label" xid="label8">开始日期</label>  
          <input component="$UI/system/components/justep/input/input" class="form-control x-edit"
            xid="startdate" dataType="Date"/> 
        </div>  
        <div component="$UI/system/components/justep/labelEdit/labelEdit" class="x-label-edit x-label30"
          xid="labelEdit8" style="width:90%;margin-left:5%;"> 
          <label class="x-label" xid="label7">结束日期</label>  
          <input component="$UI/system/components/justep/input/input" class="form-control x-edit"
            xid="enddate" dataType="Date"/> 
        </div>  
        <div xid="div11" style="width:90%;margin-left:5%;margin-top:5%;"> 
          <a component="$UI/system/components/justep/button/button" class="btn btn-info"
            label="查询" xid="button2" onClick="button2Click" style="height:100%;width:100%;border-width:thin thin thin thin;background-color:#FF6C1F;"> 
            <i xid="i6"/>  
            <span xid="span4">查询</span> 
          </a> 
        </div> 
      </div> 
    </div>  
    <div class="x-wing-content" xid="content2"> 
      <div component="$UI/system/components/justep/panel/panel" class="x-panel x-full"
        xid="panel1"> 
        <div class="x-panel-top" xid="top1" height="43"> 
          <nav class="nav center" xid="default3"> 
            <i class="left" bind-click="{operation:'window.close'}" xid="i4"/>  
            <span xid="span3">事件</span>  
            <a href="#" xid="a1"/>  
            <div class="menu-list" xid="div9"> 
              <ul xid="ul1"> 
                <li xid="li1"> 
                  <a href="#" xid="a2">地图导航</a> 
                </li>  
                <li xid="li2"> 
                  <a href="#" xid="a3">变配电站概况</a> 
                </li>  
                <li class="inspect" xid="li3"> 
                  <h2 xid="h21"> 
                    <a href="#" xid="a4">巡检管理</a>  
                    <i class="foldIcon" xid="i5"/> 
                  </h2>  
                  <ul class="menu-child" xid="ul2"> 
                    <li xid="li4"> 
                      <a href="#" xid="a5">巡检任务</a> 
                    </li>  
                    <li xid="li5"> 
                      <a href="#" xid="a6">任务执行</a> 
                    </li>  
                    <li xid="li6"> 
                      <a href="#" xid="a7">查询任务</a> 
                    </li> 
                  </ul> 
                </li>  
                <li xid="li7"> 
                  <a href="#" xid="a8">事件</a> 
                </li> 
              </ul> 
            </div> 
          </nav> 
        </div>  
        <div class="x-panel-content  x-scroll-view" xid="content1" _xid="C799CEF4D7E00001D8FC14A019972300"
          style="bottom: 0px; top: 43px;" bind-click="content1Click"> 
          <div class="x-scroll" component="$UI/system/components/justep/scrollView/scrollView"
            xid="scrollView1" autoAppend="true" onPullDown="scrollView1PullDown"> 
            <div class="x-content-center x-pull-down container" xid="div2"> 
              <i class="x-pull-down-img glyphicon x-icon-pull-down" xid="i1"/>  
              <span class="x-pull-down-label" xid="span1">下拉刷新...</span>
            </div>  
            <div class="x-scroll-content" xid="div3">
              <div xid="div1"> 
                <a component="$UI/system/components/justep/button/button"
                  class="btn btn-info" label="筛选" xid="button1" onClick="button1Click"
                  style="margin:7px 0px 0px 0px;"> 
                  <i xid="i2"/>  
                  <span xid="span2">筛选</span>
                </a>
                <div component="$UI/system/components/justep/output/output"
                  class="x-output pull-right x-edit" xid="subname" style="font-size:16px;font-weight:600;margin-top:5px;margin-right:10px;"/>
              </div>
              <div component="$UI/system/components/justep/list/list" class="x-list"
                xid="list4" limit="11" data="eventdata" disablePullToRefresh="true"
                disableInfiniteLoad="false"> 
                <table class="table table-bordered table-hover table-striped"
                  component="$UI/system/components/bootstrap/table/table" xid="table2"> 
                  <thead xid="thead4"> 
                    <tr xid="tr6"> 
                      <th xid="col11" style="text-align:center;background-color:#3DB3FF;width:110px;color:#FFFFFF;">时间</th>  
                      <th xid="col1" style="text-align:center;background-color:#3DB3FF;color:#FFFFFF;">仪表</th>  
                      <th xid="col14" style="text-align:center;background-color:#3DB3FF;color:#FFFFFF;">类型</th>  
                      <th xid="col2" style="text-align:center;background-color:#3DB3FF;color:#FFFFFF;">详情</th> 
                    </tr> 
                  </thead>  
                  <tbody class="x-list-template" xid="listTemplate3"> 
                    <tr xid="tr7"> 
                      <td xid="td7" bind-text="ref(&quot;starttime&quot;)"/>  
                      <td xid="td1" bind-text="ref(&quot;metername&quot;)"/>  
                      <td xid="td10" bind-text="ref(&quot;alarmtype&quot;)"/>  
                      <td xid="td9" style="text-align:center;"> 
                        <a component="$UI/system/components/justep/button/button"
                          class="btn btn-info" label="详情" xid="button3" onClick="button3Click"
                          style="background-color:#FF6C1F;"> 
                          <i xid="i3"/>  
                          <span xid="span8">详情</span> 
                        </a> 
                      </td> 
                    </tr> 
                  </tbody> 
                </table> 
              </div>
            </div>  
            <div class="x-content-center x-pull-up" xid="div6"></div> 
          </div>
        </div> 
      </div> 
    </div> 
  </div> 
</div>
