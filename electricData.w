<?xml version="1.0" encoding="utf-8"?>

<div xmlns="http://www.w3.org/1999/xhtml" xid="window" class="window container-fluid" component="$UI/system/components/justep/window/window"
  design="device:m;">  
  <div component="$UI/system/components/justep/model/model" xid="model" style="height:auto;top:335px;left:508px;"
    onLoad="modelLoad"> 
    <div component="$UI/system/components/justep/data/data" autoLoad="true"
      xid="subdata" idColumn="f_SubID"> 
      <column name="f_SubID" type="String" xid="xid1"></column>
  <column name="f_SubName" type="String" xid="xid2"></column>
  <data xid="default1">[]</data></div> 
  </div>  
  <html lang="zh-CN"> 
    <head> 
      <meta charset="utf-8"/>  
      <meta http-equiv="X-UA-Compatible" content="IE=edge"/>  
      <meta name="viewport" content="width=device-width, initial-scale=1"/>  
      <title>变电所运维云平台</title>  
      <link href="css/bootstrap.min.css" rel="stylesheet"/>  
      <link href="css/bootstrap-table.css" rel="stylesheet"/>  
      <link href="css/main.css" rel="stylesheet"/> 
    </head> 
  </html>  
  <div component="$UI/system/components/justep/wing/wing" class="x-wing" xid="wing1"> 
    <div class="x-wing-left" xid="left1" style="background-color:#EDEEEE;border-right-style:solid;border-right-width:1px;border-color:#898989"> 
      <div xid="div10" class="pull-left" style="position:absolute;"> 
        <div xid="div4"> 
          <img src="./image/select.jpg" alt="" xid="image1" dir="rtl" style="width:100%;"/> 
        </div>  
        <div component="$UI/system/components/justep/labelEdit/labelEdit" class="x-label-edit x-label30"
          xid="labelEdit7" style="margin-top:5%;width:90%;margin-left:5%;"> 
          <label class="x-label" xid="label6"><![CDATA[变配电站]]></label>  
          <select component="$UI/system/components/justep/select/select" class="form-control x-edit"
            xid="subid" bind-options="subdata" bind-optionsValue="f_SubID" bind-optionsLabel="f_SubName"
            onChange="subidChange"/> 
        </div>  
        <div component="$UI/system/components/justep/labelEdit/labelEdit" class="x-label-edit x-label30"
          xid="labelEdit3" style="width:90%;margin-left:5%;"> 
          <label class="x-label" xid="label2"><![CDATA[回路名称]]></label>  
          <!-- <select component="$UI/system/components/justep/select/select" class="form-control x-edit"
            xid="subid"/> -->  
          <select id="fCircuitid" class="form-control x-edit"/> 
        </div>  
        <div component="$UI/system/components/justep/labelEdit/labelEdit" class="x-label-edit x-label30"
          xid="labelEdit1" style="width:90%;margin-left:5%;"> 
          <label class="x-label" xid="label1"><![CDATA[参数类别]]></label>  
          <select component="$UI/system/components/justep/select/select" class="form-control x-edit"
            xid="select1" id="selectType"> 
           <!--  <select component="$UI/system/components/justep/select/select" class="form-control x-edit"
            xid="select1" id="selectType">  -->
            <option value="I">电流</option>  
            <option value="U">相电压</option>  
            <option value="UL">线电压</option>  
            <option value="fFr">频率</option>  
            <option value="EPI">有功电能</option>  
            <option value="P">有功功率</option>  
            <option value="Q">无功功率</option>  
            <option value="S">视在功率</option>
            <option value="PF">功率因数</option>
            <option value="UH">电压总谐波含量</option>
            <option value="IH">电流总谐波含量</option>
          </select> 
        </div>  
        <div component="$UI/system/components/justep/labelEdit/labelEdit" class="x-label-edit x-label30 pull-left"
          xid="labelEdit6" style="left:10%;width:90%;margin-left:5%;"> 
          <label class="x-label" xid="label8"><![CDATA[日期]]></label>  
          <input component="$UI/system/components/justep/input/input" class="form-control x-edit"
            xid="startdate" dataType="Date"/> 
        </div>  
        <div xid="div11" style="width:90%;margin-left:5%;margin-top:5%;"> 
          <a component="$UI/system/components/justep/button/button" class="btn btn-info"
            label="查询" xid="button2" onClick="button2Click" style="height:100%;width:100%;border-width:thin thin thin thin;background-color:#FF6C1F;"> 
            <i xid="i6"/>  
            <span xid="span4">查询</span> 
          </a> 
        </div> 
      </div> 
    </div>  
    <div class="x-wing-content" xid="content2"> 
      <div component="$UI/system/components/justep/panel/panel" class="x-panel x-full"
        xid="panel1"> 
        <div class="x-panel-top" xid="top1" height="43"> 
          <nav class="nav center" xid="default3"> 
            <i class="left" bind-click="{operation:'window.close'}" xid="i4"/>  
            <span xid="span3"><![CDATA[电力参数]]></span>  
            <a href="#" xid="a1"/>  
            <div class="menu-list" xid="div9"> 
              <ul xid="ul1"> 
                <li xid="li1"> 
                  <a href="#" xid="a2">地图导航</a> 
                </li>  
                <li xid="li2"> 
                  <a href="#" xid="a3">变配电站概况</a> 
                </li>  
                <li class="inspect" xid="li3"> 
                  <h2 xid="h21"> 
                    <a href="#" xid="a4">巡检管理</a>  
                    <i class="foldIcon" xid="i5"/> 
                  </h2>  
                  <ul class="menu-child" xid="ul2"> 
                    <li xid="li4"> 
                      <a href="#" xid="a5">巡检任务</a> 
                    </li>  
                    <li xid="li5"> 
                      <a href="#" xid="a6">任务执行</a> 
                    </li>  
                    <li xid="li6"> 
                      <a href="#" xid="a7">查询任务</a> 
                    </li> 
                  </ul> 
                </li>  
                <li xid="li7"> 
                  <a href="#" xid="a8">事件</a> 
                </li> 
              </ul> 
            </div> 
          </nav> 
        </div>  
        <div class="x-panel-content  x-scroll-view" xid="content1" _xid="C799CEF4D7E00001D8FC14A019972300"
          style="bottom: 0px; top: 43px;" bind-click="content1Click"> 
          
          <div class="x-scroll-content" xid="div3"> 
              <div xid="div1"> 
                <a component="$UI/system/components/justep/button/button"
                  class="btn btn-info" label="筛选" xid="button1" onClick="button1Click"
                  style="margin:7px 0px 0px 0px;"> 
                  <i xid="i2"/>  
                  <span xid="span2">筛选</span> 
                </a>  
                <div component="$UI/system/components/justep/output/output"
                  class="x-output pull-right x-edit" xid="subname" style="font-size:16px;font-weight:600;margin-top:5px;margin-right:10px;"/>  
                 <div class="Datarow">
                  <div component="$UI/system/components/justep/output/output"
                  class="pull-right" xid="time" id="time" style="margin-right:8px;"/>  
                  <div id="HLname" class="pull-right" style="margin-right:10px;"/> 
                 </div>
              </div>  
              <!-- bootstrap table -->  
              <div id="tableData"> 
              </div>
            </div>  
        </div> 
      </div> 
    </div> 
  </div> 
</div>
