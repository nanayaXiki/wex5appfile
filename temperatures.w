<?xml version="1.0" encoding="utf-8"?>

<div xmlns="http://www.w3.org/1999/xhtml" xid="window" class="window" component="$UI/system/components/justep/window/window"
  design="device:m;">  
  <div component="$UI/system/components/justep/model/model" xid="model" style="height:auto;top:241px;left:606px;"><div component="$UI/system/components/justep/data/data" autoLoad="true" xid="subdata" idColumn="f_Value">
  <!-- <column label="f_text2" name="col4" type="String" xid="xid5"></column>
  <column label="f_text3" name="col5" type="String" xid="xid6"></column>  -->
  <column label="f_Value" name="f_Value" type="String" xid="xid1"></column>
  <column isCalculate="false" label="F_MeterName" name="F_MeterName" type="String" xid="xid2"></column>
  <column isCalculate="false" label="temp" name="temp" type="String" xid="xid3"></column>
  <column isCalculate="false" label="humi" name="humi" type="String" xid="xid4"></column></div>
  </div>  
  <html lang="zh-CN"> 
    <head> 
      <meta charset="utf-8"/>  
      <meta http-equiv="X-UA-Compatible" content="IE=edge"/>  
      <meta name="viewport" content="width=device-width, initial-scale=1"/>  
      <title>变电所运维云平台</title>  
      <link href="css/bootstrap.min.css" rel="stylesheet"/>  
      <link href="css/main.css" rel="stylesheet"/> 
    </head>  
   
  </html>  
  <div component="$UI/system/components/justep/panel/panel" class="x-panel x-full" xid="panel1">
   <div class="x-panel-content  x-scroll-view" xid="content1" _xid="C794C5517C800001E4981750113B3090"><div class="x-scroll" component="$UI/system/components/justep/scrollView/scrollView" xid="scrollView1" autoAppend="true">
   <div class="x-content-center x-pull-down container" xid="div1">
    <i class="x-pull-down-img glyphicon x-icon-pull-down" xid="i1"></i>
    <span class="x-pull-down-label" xid="span1">下拉刷新...</span></div> 
   <div class="x-scroll-content" xid="div2"><div component="$UI/system/components/justep/list/list" class="x-list" xid="list4" data="subdata" limit="10"> 
          <table class="table table-bordered table-hover table-striped" component="$UI/system/components/bootstrap/table/table" xid="table2"> 
            <thead xid="thead4"> 
              <tr xid="tr6"> 
                <th xid="col11" style="width:166px;height:40px;text-align:center;background-color:#3DB3FF;color:#FFFFFF;vertical-align:middle;">仪表名称</th>  
                
                <!-- <th xid="col12" style="width:165px;text-align:center;background-color:#3DB3FF;color:#FFFFFF;"><![CDATA[有功功率]]></th>  
                <th xid="col13" style="width:165px;text-align:center;background-color:#3DB3FF;color:#FFFFFF;">无功功率</th>  -->
                <th xid="col14" style="width:165px;text-align:center;background-color:#3DB3FF;color:#FFFFFF;vertical-align:middle;">环境温度(℃)</th> 
                 <th xid="col15" style="width:165px;text-align:center;background-color:#3DB3FF;color:#FFFFFF;vertical-align:middle;">环境湿度(%)</th> 
              </tr> 
            </thead>  
            <tbody class="x-list-template" xid="listTemplate3"> 
              <tr xid="tr7"> 
                <td xid="td7" bind-text='ref("F_MeterName")' style="text-align:center;"/>  
                <td xid="td8" bind-text='ref("temp")' style="text-align:center;" />  
                <td xid="td9" bind-text ='ref("humi")'  style="text-align:center;">
              <!--   <td xid= "td10" bind-text='ref("f_test2")' /> 
                <td xid= "td11" bind-text='ref("f_test3")' />  -->
                  </td> 
              </tr> 
            </tbody> 
          </table> 
        </div></div>
   <div class="x-content-center x-pull-up" xid="div3">
    </div> </div></div>
   </div>
  <span component="$UI/system/components/justep/windowReceiver/windowReceiver" xid="windowReceiver1" onReceive="windowReceiver1Receive"></span></div>
