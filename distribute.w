<?xml version="1.0" encoding="utf-8"?>

<div xmlns="http://www.w3.org/1999/xhtml" xid="window" class="window" component="$UI/system/components/justep/window/window"
  design="device:m;">  
  <div component="$UI/system/components/justep/model/model" xid="model" style="height:auto;top:351px;left:577px;"> 
     
  </div>  
  <html lang="zh-CN"> 
      <head> 
        <meta charset="utf-8" />  
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />  
        <meta name="viewport" content="width=device-width, initial-scale=1" />  
         <link href="css/bootstrap-select.css" rel="stylesheet" /> 
        <link href="css/bootstrap.min.css" rel="stylesheet" />  
        <link href="css/main.css" rel="stylesheet" />  
       
        
      </head>  
      <body> 
        <div class="content-fluid distribute-main"> 
          <img src="image/people.png" class="people" />  
          <div class="row"> 
          	 <span id="alert" style="text-align:center;color:#FF0000;" class="center-block"></span>
            <div class="col-xs-12 distribute"> 
              <label>巡检人：</label>  
              <div class="btn-group" style="border-color:#FF0000 #FF0000 #FF0000 #FF0000;border-width:2px 2px 2px 2px;"> 
                <select id="select" name="select" class="selectpicker form-control" multiple="multiple" data-live-search="false" xid="select" size="1" title="--请选择--"> 
               
                </select> 
              </div> 
            </div>  
            <div class="col-xs-12 distribute"> 
              <label>完成日期：</label>  
              <div id="dayCalendar" class="input-group date form_date count-data" data-date="" data-date-format="dd MM yyyy" data-link-field="dtp_input2" data-link-format="yyyy-mm-dd"> 
                <input component="$UI/system/components/justep/input/input" class="form-control" xid="datetime" dataType="Date" format="yyyy-MM-dd"></input></div> 
            </div> 
          </div>  
          <div class="row"> 
            <div class="col-xs-6 dis-btn"> 
              <button type="button" xid="button1" bind-click="button1Click"><![CDATA[取消]]></button> 
            </div>  
            <div class="col-xs-6 dis-btn"> 
              <button type="button" xid="button2" bind-click="button2Click">确定</button> 
            </div> 
          </div> 
        </div> 
       
      </body> 
    </html><span component="$UI/system/components/justep/windowReceiver/windowReceiver" xid="windowReceiver1" onReceive="windowReceiver1Receive"></span>
  </div>
