<?xml version="1.0" encoding="utf-8"?>

<div xmlns="http://www.w3.org/1999/xhtml" xid="window" class="window" component="$UI/system/components/justep/window/window"
  design="device:m;">  
  <div component="$UI/system/components/justep/model/model" xid="model" style="height:auto;top:241px;left:606px;">
  <div component="$UI/system/components/justep/data/data" autoLoad="true" xid="subdata" idColumn="f_SubID">
  <column label="f_SubID" name="f_SubID" type="String" xid="xid1"></column>
  <column label="subName" name="subName" type="String" xid="xid2"></column>
  <column label="PValue" name="PValue" type="String" xid="xid3"></column>
  <column label="QValue" name="QValue" type="String" xid="xid4"></column>
  <column label="PFValue" name="PFValue" type="String" xid="xid5"></column>
  <column label="LFValue" name="LFValue" type="String" xid="xid7"></column></div>
  </div>  
  <html lang="zh-CN"> 
    <head> 
      <meta charset="utf-8"/>  
      <meta http-equiv="X-UA-Compatible" content="IE=edge"/>  
      <meta name="viewport" content="width=device-width, initial-scale=1"/>  
      <title>变电所运维云平台</title>  
      <link href="css/bootstrap.min.css" rel="stylesheet"/>  
      <link href="css/main.css" rel="stylesheet"/> 
    </head>  
   
  </html>  
  <span component="$UI/system/components/justep/windowDialog/windowDialog" xid="windowDialog1"
    src="$UI/app/distribute.w" status="normal" width="80%" height="60%" style="top:228px;left:281px;"/> 
<div component="$UI/system/components/justep/panel/panel" class="x-panel x-full" xid="panel1">
   <div class="x-panel-content  x-scroll-view" xid="content1" _xid="C794C5517C800001E4981750113B3090"><div class="x-scroll" component="$UI/system/components/justep/scrollView/scrollView" xid="scrollView1" autoAppend="true">
   <div class="x-content-center x-pull-down container" xid="div1">
    <i class="x-pull-down-img glyphicon x-icon-pull-down" xid="i1"></i>
    <span class="x-pull-down-label" xid="span1">下拉刷新...</span></div> 
   <div class="x-scroll-content" xid="div2"><div component="$UI/system/components/justep/list/list" class="x-list" xid="list4" data="subdata" limit="10"> 
          <table class="table table-bordered table-hover table-striped" component="$UI/system/components/bootstrap/table/table" xid="table2"> 
            <thead xid="thead4"> 
              <tr xid="tr6"> 
                <th xid="col11" style="width:100px;height:40px;text-align:center;background-color:#3DB3FF;color:#FFFFFF;font-size:12px;vertical-align:middle;">变压器名称</th>  
                <th xid="col12" style="width:100px;text-align:center;background-color:#3DB3FF;color:#FFFFFF;font-size:12px;vertical-align:middle;"><![CDATA[有功功率(kW)]]></th>  
                <th xid="col13" style="width:100px;text-align:center;background-color:#3DB3FF;color:#FFFFFF;font-size:12px;vertical-align:middle;">无功功率(kVar)</th> 
                <th xid="col14" style="width:100px;text-align:center;background-color:#3DB3FF;color:#FFFFFF;font-size:12px;vertical-align:middle;">功率因数</th> 
                <th xid="col15" style="width:70px;text-align:center;background-color:#3DB3FF;color:#FFFFFF;font-size:12px;vertical-align:middle;">负荷率(%)</th> 
              </tr> 
            </thead>  
            <tbody class="x-list-template" xid="listTemplate3"> 
              <tr xid="tr7"> 
                <td xid="td7" bind-text='ref("subName")' style="text-align:center;"/>  
                <td xid="td8" bind-text='ref("PValue")' style="text-align:center;"/>  
                <td xid="td9" bind-text='ref("QValue")' style="text-align:center;"></td> 
                <td xid= "td10" bind-text='ref("PFValue")' style="text-align:center;"/> 
                <td xid= "td11" bind-text='ref("LFValue")' style="text-align:center;"/> 
              </tr> 
            </tbody> 
          </table> 
        </div></div>
   <div class="x-content-center x-pull-up" xid="div3">
    </div> </div></div>
   </div>
  <span component="$UI/system/components/justep/windowReceiver/windowReceiver" xid="windowReceiver1" onReceive="windowReceiver1Receive"></span></div>
