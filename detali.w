<?xml version="1.0" encoding="utf-8"?>

<div xmlns="http://www.w3.org/1999/xhtml" xid="window" class="window container-fluid" component="$UI/system/components/justep/window/window"
  design="device:m;" style="width: 100%; height: 100%;">  
  <div component="$UI/system/components/justep/model/model" xid="model" style="height:auto;top:69px;left:636px;" onParamsReceive="modelParamsReceive"></div>  
  <html lang="zh-CN"> 
    <head> 
      <meta charset="utf-8"/>  
      <meta http-equiv="X-UA-Compatible" content="IE=edge"/>  
      <meta name="viewport" content="width=device-width, initial-scale=1"/>  
      <title>变电所运维云平台</title>  
      <link href="css/bootstrap.min.css" rel="stylesheet"/>  
      <link href="css/main.css" rel="stylesheet"/> 
    </head>  
   
  </html>  
  <span component="$UI/system/components/justep/windowDialog/windowDialog" xid="windowDialog1" width="90%" src="$UI/app/temperatures.w" title="环境温湿度" showTitle="true" status="normal"></span><div component="$UI/system/components/justep/panel/panel" class="x-panel x-full"
    xid="panel1"> 
    <div class="x-panel-top" xid="top1" height="43"><nav class="nav center" xid="default1"> 
              <i class="left" bind-click="{operation:'window.close'}" xid="i3"/>  
              <span xid="span5"><![CDATA[变配电站详情]]></span>  
              <a href="#" xid="a1"/>  
              <div class="menu-list" xid="div8"> 
                <ul xid="ul1"> 
                  <li xid="li1"> 
                    <a href="#" xid="a2">地图导航</a>
                  </li>  
                  <li xid="li2"> 
                    <a href="#" xid="a3">变配电站概况</a>
                  </li>  
                  <li class="inspect" xid="li3"> 
                    <h2 xid="h21"> 
                      <a href="#" xid="a4">巡检管理</a>  
                      <i class="foldIcon" xid="i4"/>
                    </h2>  
                    <ul class="menu-child" xid="ul2"> 
                      <li xid="li4"> 
                        <a href="#" xid="a5">巡检任务</a>
                      </li>  
                      <li xid="li5"> 
                        <a href="#" xid="a6">任务执行</a>
                      </li>  
                      <li xid="li6"> 
                        <a href="#" xid="a7">查询任务</a>
                      </li> 
                    </ul> 
                  </li>  
                  <li xid="li7"> 
                    <a href="#" xid="a8">事件</a>
                  </li> 
                </ul> 
              </div> 
            </nav> </div>  
    <div class="x-panel-content  x-scroll-view" xid="content1" _xid="C7970210E1D000013262186ED6302600">
      <div class="x-scroll" component="$UI/system/components/justep/scrollView/scrollView"
        xid="scrollView2" autoPullUp="false"> 
        <div class="x-content-center x-pull-down container" xid="div4"> 
          <i class="x-pull-down-img glyphicon x-icon-pull-down" xid="i2"/></div>  
        <div class="x-scroll-content" xid="div5">
          <div class="content-fluid" xid="div7"> 
            <div class="contain" xid="div9"> 
              <div class="detail-box" xid="div10" id="div111"> 
                <h4 xid="h41">变配电站概况</h4>  
                <h3 id="fsubName" xid="h31"/>  
                <ul class="detail-ul" xid="ul3"> 
                  <li xid="li8"> 
                    <span class="level" xid="span6"/>  
                    <p xid="p1">电压等级</p>  
                    <div class="box-bottom" xid="div11"> 
                      <p id="level" xid="p2"/>
                    </div> 
                  </li>  
                  <li xid="li9" bind-click="liTFClick"> 
                    <span class="number" xid="span7"/>  
                    <p xid="p3">变压器台数</p>  
                    <div class="box-bottom" xid="div12"> 
                      <p id="number" xid="p4"/>
                    </div> 
                  </li>  
                  <li xid="li10"> 
                    <span class="install" xid="span8"/>  
                    <p xid="p5">装机容量</p>  
                    <div class="box-bottom" xid="div13"> 
                      <p id="install" xid="p6"> 
                        <span xid="span9">kVA</span>
                      </p> 
                    </div> 
                  </li>  
                  <li xid="li11"> 
                    <span class="operation" xid="span10"/>  
                    <p xid="p7">负荷率</p>  
                    <div class="box-bottom" xid="div14"> 
                      <p id="operation" xid="p8"> 
                        <span xid="span11">%</span>
                      </p> 
                    </div> 
                  </li>  
                  <li xid="li12"> 
                    <span class="observe" xid="span12"/>  
                    <p xid="p9">测控装置</p>  
                    <div class="box-bottom" xid="div15"> 
                      <p id="observe" xid="p10"> 
                        <span xid="span13">个</span>
                      </p> 
                    </div> 
                  </li> 
                </ul> 
              </div> 
            </div>
            <div class="contain" xid="div16"> 
              <div class="detail-box" xid="div17"> 
                <h4 xid="h42">运行状态</h4>  
                <p class="reset-time" id="reset-time" xid="p11"/>  
                <ul class="operation" xid="ul4"> 
                  <li xid="li13"> 
                    <span class="perform" xid="span14"/>  
                    <p xid="p12">总有功功率</p>  
                    <p class="perform-p" id="perform" xid="p13"> 
                      <span xid="span15">kW</span>
                    </p> 
                  </li>  
                  <li xid="li14"> 
                    <span class="idle" xid="span16"/>  
                    <p xid="p14">总无功功率</p>  
                    <p class="perform-p" id="idle" xid="p15"> 
                      <span xid="span17">kVar</span>
                    </p> 
                  </li>  
                  <li xid="li15" bind-click="liTeClick"> 
                    <span class="temperature" xid="span18"/>  
                    <p xid="p16">环境温度</p>  
                    <p class="perform-p" id="temperature" xid="p17"> 
                      <span xid="span19">℃</span>
                    </p> 
                  </li>  
                  <li xid="li16" bind-click="liTeClick2"> 
                    <span class="humidity" xid="span20"/>  
                    <p xid="p18">环境湿度</p>  
                    <p class="perform-p" id="humidity" xid="p19"> 
                      <span xid="span21">%</span>
                    </p> 
                  </li> 
                </ul> 
              </div> 
            </div>  
            <div class="contain" xid="div18"> 
              <div class="detail-box" xid="div19"> 
                <h4 xid="h43">当日事件记录</h4>  
                <ul class="operation incident" xid="ul5"> 
                  <li xid="li17"> 
                    <span class="limit" xid="span22"/>  
                    <p xid="p20">遥测越限</p>  
                    <p class="perform-p" id="limit" xid="p21"> 
                      <span xid="span23">次</span>
                    </p> 
                  </li>
                  <li xid="li18"> 
                    <span class="deflection" xid="span24"/>  
                    <p xid="p22">遥信变位</p>  
                    <p class="perform-p" id="deflection" xid="p23"> 
                      <span xid="span25">次</span>
                    </p> 
                  </li>  
                  <li xid="li19"> 
                    <span class="water" xid="span26"/>  
                    <p xid="p24">水浸</p>  
                    <p class="perform-p" id="water" xid="p25"> 
                      <span xid="span27">无</span>
                    </p> 
                  </li>  
                  <li xid="li20"> 
                    <span class="smoke" xid="span28"/>  
                    <p xid="p26">烟雾</p>  
                    <p class="perform-p" id="smoke" xid="p27"> 
                      <span xid="span29">无</span>
                    </p> 
                  </li> 
                </ul> 
              </div> 
            </div>  
            <div class="contain" xid="div20"> 
              <div class="detail-box" xid="div21"> 
                <h4 xid="h44">用电概况</h4>  
                <div class="main-box5" xid="div22"> 
                  <span class="box5-1" xid="span30">当日用电</span>  
                  <span id="triangle-right" xid="span31"/>  
                  <span id="todayTotal" class="box5-1b" xid="span32"/>
                </div>  
                <div class="main-box5" xid="div23"> 
                  <span class="box5-1 box5-2" xid="span33">昨日同期</span>  
                  <span id="triangle-right" class="triangle-right2" xid="span34"/>  
                  <span id="yesterdayTotal" class="box5-1b" xid="span35"/>
                </div>  
                <div class="main-box5" xid="div24"> 
                  <span class="box5-1 box5-3" xid="span36">环 比</span>  
                  <span id="triangle-right" class="triangle-right3" xid="span37"/>  
                  <span id="difference" class="box5-2b" xid="span38"/>
                </div>  
                <div class="main-box5c" xid="div25"> 
                  <div class="box5c1" xid="div26"> 
                    <p id="maxTime" xid="p28"/>  
                    <p xid="p29">最大用电时间</p>
                  </div>  
                  <div class="box5c1" xid="div27"> 
                    <p id="levelPower" xid="p30"/>  
                    <p xid="p31">该时段平均功率</p>
                  </div> 
                </div> 
              </div>
            </div>
            <div class="contain" >
            	<div class="detail-box">
            		<h4><![CDATA[当日逐时用电趋势图]]></h4>
            		 <div xid="echarts" id="domEcharts"  style="width:100%;height:400px" class="mainEcharts"></div>
            	</div>
            </div>
          </div>
        </div>  
        </div>
    </div>  
    </div>
<span component="$UI/system/components/justep/windowDialog/windowDialog" xid="windowDialog2" src="$UI/app/transformers.w" title="变压器列表" showTitle="true" width="90%" status="normal"></span></div>
