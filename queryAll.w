<?xml version="1.0" encoding="utf-8"?>

<div xmlns="http://www.w3.org/1999/xhtml" xid="window" class="window" component="$UI/system/components/justep/window/window"
  design="device:m;" style="width: 100%; height: 100%;">  
  <div component="$UI/system/components/justep/model/model" xid="model" style="height:auto;top:251px;left:624px;" onActive="modelActive" onLoad="modelParamsReceive"><div component="$UI/system/components/justep/data/data" autoLoad="true" xid="plandata" idColumn="planid"><column label="planID" name="planid" type="String" xid="xid1"></column>
  <column label="subName" name="subname" type="String" xid="xid2"></column>
  <column label="addRess" name="address" type="String" xid="xid3"></column>
  <column label="username" name="username" type="String" xid="xid4"></column>
  <column label="deadLine" name="endtime" type="DateTime" xid="xid5"></column>
  <column label="completetime" name="completetime" type="DateTime" xid="xid6"></column>
  <rule xid="rule1">
   <col name="username" xid="ruleCol1">
    <constraint xid="constraint1">
     <expr xid="default1"></expr></constraint> 
    <readonly xid="readonly1">
     <expr xid="default2"></expr></readonly> </col> </rule></div>
  </div>  
    
  <html lang="zh-CN"> 
    <head> 
      <meta charset="utf-8" />  
      <meta http-equiv="X-UA-Compatible" content="IE=edge" />  
      <meta name="viewport" content="width=device-width, initial-scale=1" />  
      <title>变电所运维云平台</title>  
      <link href="css/bootstrap.min.css" rel="stylesheet" />  
      <link href="css/main.css" rel="stylesheet" /> 
    </head>  
  
  </html>
  <div component="$UI/system/components/justep/panel/panel" class="x-panel x-full" xid="panel1">
   <div class="x-panel-top" xid="top1" height="43"><nav class="nav center"> 
          <i class="left" bind-click="{operation:'window.close'}"/>  
          <span>查询任务</span>  
          <a href="#"> 
          <!--   <img src="image/menu.png" class="menu" />  -->
          </a>  
          <div class="menu-list"> 
            <ul> 
              <li> 
                <a href="#">地图导航</a> 
              </li>  
              <li> 
                <a href="#">变配电站概况</a> 
              </li>  
              <li class="inspect"> 
                <h2> 
                  <a href="#">巡检管理</a>  
                  <i class="foldIcon" /> 
                </h2>  
                <ul class="menu-child"> 
                  <li> 
                    <a href="#">巡检任务</a> 
                  </li>  
                  <li> 
                    <a href="#">任务执行</a> 
                  </li>  
                  <li> 
                    <a href="#">查询任务</a> 
                  </li> 
                </ul> 
              </li>  
              <li> 
                <a href="#">事件</a> 
              </li> 
            </ul> 
          </div> 
        </nav>  </div>
   <div class="x-panel-content  x-scroll-view" xid="content3" _xid="C794C25CD59000018B9E1A4016808E00">
  <div class="x-scroll" component="$UI/system/components/justep/scrollView/scrollView" xid="scrollView2" onPullDown="modelActive">
   <div class="x-content-center x-pull-down container" xid="div4">
    <i class="x-pull-down-img glyphicon x-icon-pull-down" xid="i2"></i>
    <span class="x-pull-down-label" xid="span3">下拉刷新...</span></div> 
   <div class="x-scroll-content" xid="div5"><div component="$UI/system/components/justep/list/list" class="x-list" xid="list4" data="plandata" limit="10"> 
          <table class="table table-bordered table-hover table-striped" component="$UI/system/components/bootstrap/table/table" xid="table2"> 
            <thead xid="thead4"> 
              <tr xid="tr6"> 
                <th xid="col11" style="text-align:center;background-color:#3DB3FF;color:#FFFFFF;width:20%;"><![CDATA[变配电站]]></th>  
                 
                <th xid="col12" style="text-align:center;background-color:#3DB3FF;color:#FFFFFF;width:21%;">地点</th>
                <th xid="col12" style="text-align:center;background-color:#3DB3FF;color:#FFFFFF;width:19%;">巡检人</th>  
                <th xid="col14" style="text-align:center;background-color:#3DB3FF;color:#FFFFFF;width:20%;">计划完成日期</th>
                <th xid="col14" style="text-align:center;background-color:#3DB3FF;color:#FFFFFF;">实际完成日期</th> 
                
              </tr> 
            </thead>  
            <tbody class="x-list-template" xid="listTemplate3"> 
              <tr xid="tr7"> 
                <td xid="td7" bind-text='ref("subname")'></td>  
                <td xid="td10" bind-text='ref("address")' />  
                <td xid="td8" bind-text='ref("username")' />  
                <td xid="td9" style="text-align:center;" bind-text='ref("completetime")'>
                  </td> 
              <td xid="td1" bind-text='ref("endtime")'></td></tr> 
            </tbody> 
          </table> 
        </div></div>
   <div class="x-content-center x-pull-up" xid="div6">
   </div></div></div>
   </div></div>
