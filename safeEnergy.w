<?xml version="1.0" encoding="utf-8"?>

<div xmlns="http://www.w3.org/1999/xhtml" xid="window" class="window" component="$UI/system/components/justep/window/window"
  design="device:m;">  
  <div component="$UI/system/components/justep/model/model" xid="model" onLoad="modelLoad"
    style="top:462px;left:12px;height:auto;"> 
    <div component="$UI/system/components/justep/data/data" autoLoad="true"
      xid="subdata" idColumn="f_SubID"> 
      <column name="f_SubID" type="String" xid="xid1"/>  
      <column name="f_SubName" type="String" xid="xid2"/> 
    </div>  
    <div component="$UI/system/components/justep/data/data" autoLoad="true"
      xid="tempdata" idColumn="f_SubID"> 
      <column label="f_SubID" name="f_SubID" type="Integer" xid="xid6"/>  
      <column label="f_SubName" name="f_SubName" type="String" xid="xid7"/>  
      <column label="a" name="a" type="String" xid="xid8"/>  
      <column label="b" name="b" type="String" xid="xid9"/>  
      <column label="c" name="c" type="String" xid="xid10"/>  
      <column label="f_MeterCode" name="f_MeterCode" type="String" xid="xid3"/>  
      <column label="f_MeterName" name="f_MeterName" type="String" xid="xid4"/> 
    </div> 
  </div>  
  <html lang="zh-CN"> 
    <head> 
      <meta charset="utf-8"/>  
      <meta http-equiv="X-UA-Compatible" content="IE=edge"/>  
      <meta name="viewport" content="width=device-width, initial-scale=1"/>  
      <title>变电所运维云平台</title>  
      <link href="css/bootstrap.min.css" rel="stylesheet"/>  
      <link href="css/main.css" rel="stylesheet"/> 
    </head> 
  </html>  
  <div class="content-fluid"> 
    <nav class="nav center"> 
      <i class="left" bind-click="{operation:'window.close'}"/>  
      <span>线缆温度</span>  
      <a href="#"> 
        </a> 
    </nav>  
    <div> 
 
      <div class="Line-Temp"> 
        <div class="x-gridSelect" component="$UI/system/components/justep/gridSelect/gridSelect" xid="gridSelect2" inputFilterable="true" multiselect="false" clearButton="false" onUpdateValue="gridSelect2UpdateValue">
        <option xid="option2" data="subdata" showRowNumber="false" value="f_SubID" label="f_SubName"></option></div></div> 
      </div> 
    <span xid="subName" class="subName"></span><div component="$UI/system/components/justep/list/list" class="x-list"
      xid="list1" data="tempdata" id="list1"> 
      <ul class="x-list-template" xid="listTemplateUl1"> 
        <li xid="li1" class="temDiv2"> 
          <div class="tempDiv temperature"> 
            <h4 bind-text="ref(&quot;f_MeterName&quot;)"/>  
            <div class="tempPic"> 
              <img src="image/temp.png"/>  
              </div>  
            <div class="tempNum"> 
              <p>A:
                <span bind-text="ref(&quot;a&quot;)"/>℃
              </p>  
              <p>B:
                <span bind-text="ref(&quot;b&quot;)"/>℃
              </p>  
              <p>C:
                <span bind-text="ref(&quot;c&quot;)"/>℃
              </p> 
            </div>  
            <button class="search" type="button" bind-click="tempBtnClick">查 询</button> 
          </div> 
        </li> 
      </ul> 
    </div> 
  </div>  
  <span component="$UI/system/components/justep/windowDialog/windowDialog" xid="windowDialog2"
    src="$UI/app/safeDetails.w" title="历史数据" showTitle="true" status="normal" height="80%" width="100%"/> 
</div>
