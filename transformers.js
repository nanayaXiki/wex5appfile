define(function(require) {
	var $ = require("jquery");
	var justep = require("$UI/system/lib/justep");

	var Model = function() {
		this.callParent();
	};

	var subdata;
	var ipAdress;
	
	Model.prototype.li1Click = function(event) {
		justep.Shell.closeAllOpendedPages();
	};

	Model.prototype.li2Click = function(event){
		justep.Shell.showPage("alarm");
	};
	
	Model.prototype.windowReceiver1Receive = function(event){
		userid = JSON.parse(localStorage.getItem("userUUID")).userid;
		subdata = this.comp("subdata");
		ipAdress = localStorage.getItem("ipAdress");
		subid = event.data;
		var url = "http://" + ipAdress + "/SubstationOperation/rest/app/getOpTranInfoParam";	
		$.ajax({
			type : "get",
			async : false,
			data : "fParamCodes=S,Q,P,Pf,loadFactors&fSubid=" + subid,
			url : url,
			cache : false,
			dataType : "jsonp",
			jsonp : "Callback",
			jsonpCallback : "successCallback",
			success : function(success) {
				subdata.clear();
				if (success != null)
				var table = [];
				 $.each(success,function(key,val){
				  var row = {};
				  row.subName = val.f_TransName;
				  if(val.map.P!=undefined)
					  row.PValue = parseFloat(val.map.P).toFixed(2);
				  if(val.map.Q!=undefined)
					  row.QValue = parseFloat(val.map.Q).toFixed(2);
				  if(val.map.PF!=undefined)
					  row.PFValue = parseFloat(val.map.PF).toFixed(2);
				  if(val.loadFactor!=undefined)
					  row.LFValue = parseFloat(val.loadFactor).toFixed(2);
				  table.push(row);
				 });
				 subdata.loadData(table);
			}
		});
	};
	
	return Model;
});