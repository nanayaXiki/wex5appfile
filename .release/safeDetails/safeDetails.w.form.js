define(function(require){
require('$model/UI2/system/components/justep/model/model');
require('$model/UI2/system/components/justep/loadingBar/loadingBar');
require('$model/UI2/system/components/justep/windowReceiver/windowReceiver');
require('$model/UI2/system/components/fragment/list/listTable');
require('$model/UI2/system/components/bootstrap/table/table');
require('$model/UI2/system/components/justep/input/input');
require('$model/UI2/system/components/justep/list/list');
require('$model/UI2/system/components/justep/data/data');
require('$model/UI2/system/components/justep/window/window');
var __parent1=require('$model/UI2/system/lib/base/modelBase'); 
var __parent0=require('$model/UI2/app/safeDetails'); 
var __result = __parent1._extend(__parent0).extend({
	constructor:function(contextUrl){
	this.__sysParam='true';
	this.__contextUrl=contextUrl;
	this.__id='';
	this.__cid='cz22eym';
	this._flag_='201e48c44cc2a8d6caaa149c12fc172c';
	this._wCfg_={};
	this._appCfg_={};
	this.callParent(contextUrl);
 var __Data__ = require("$UI/system/components/justep/data/data");new __Data__(this,{"autoLoad":true,"confirmDelete":true,"confirmRefresh":true,"defCols":{"f_CollectTime":{"define":"f_CollectTime","label":"f_CollectTime","name":"f_CollectTime","relation":"f_CollectTime","type":"String"},"f_MeterName":{"define":"f_MeterName","label":"f_MeterName","name":"f_MeterName","relation":"f_MeterName","type":"String"},"f_TempA":{"define":"f_TempA","label":"f_TempA","name":"f_TempA","relation":"f_TempA","type":"String"},"f_TempB":{"define":"f_TempB","label":"f_TempB","name":"f_TempB","relation":"f_TempB","type":"String"},"f_TempC":{"define":"f_TempC","label":"f_TempC","name":"f_TempC","relation":"f_TempC","type":"String"}},"directDelete":false,"events":{},"idColumn":"f_CollectTime","isMain":false,"limit":20,"xid":"subdata"});
}}); 
return __result;});