define(function(require){
require('$model/UI2/system/components/justep/model/model');
require('$model/UI2/system/components/justep/loadingBar/loadingBar');
require('$model/UI2/system/components/bootstrap/table/table');
require('$model/UI2/system/components/justep/scrollView/scrollView');
require('$model/UI2/system/components/justep/list/list');
require('$model/UI2/system/components/justep/panel/child');
require('$model/UI2/system/components/justep/data/data');
require('$model/UI2/system/components/justep/window/window');
require('$model/UI2/system/components/justep/panel/panel');
var __parent1=require('$model/UI2/system/lib/base/modelBase'); 
var __parent0=require('$model/UI2/app/alarm'); 
var __result = __parent1._extend(__parent0).extend({
	constructor:function(contextUrl){
	this.__sysParam='true';
	this.__contextUrl=contextUrl;
	this.__id='';
	this.__cid='cAZny6f';
	this._flag_='48cc2e80ccd8d60ce6a38a31acde0196';
	this._wCfg_={};
	this._appCfg_={};
	this.callParent(contextUrl);
 var __Data__ = require("$UI/system/components/justep/data/data");new __Data__(this,{"autoLoad":true,"confirmDelete":true,"confirmRefresh":true,"defCols":{"alarmdesc":{"define":"alarmdesc","name":"alarmdesc","relation":"alarmdesc","type":"String"},"alarmtype":{"define":"alarmtype","name":"alarmtype","relation":"alarmtype","type":"String"},"metername":{"define":"metername","name":"metername","relation":"metername","type":"String"},"starttime":{"define":"starttime","name":"starttime","relation":"starttime","type":"String"},"subname":{"define":"subname","name":"subname","relation":"subname","type":"String"}},"directDelete":false,"events":{},"idColumn":"subname","isMain":false,"limit":20,"xid":"noreadeventdata"});
}}); 
return __result;});