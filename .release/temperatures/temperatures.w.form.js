define(function(require){
require('$model/UI2/system/components/justep/model/model');
require('$model/UI2/system/components/justep/loadingBar/loadingBar');
require('$model/UI2/system/components/justep/windowReceiver/windowReceiver');
require('$model/UI2/system/components/bootstrap/table/table');
require('$model/UI2/system/components/justep/scrollView/scrollView');
require('$model/UI2/system/components/justep/list/list');
require('$model/UI2/system/components/justep/panel/child');
require('$model/UI2/system/components/justep/data/data');
require('$model/UI2/system/components/justep/window/window');
require('$model/UI2/system/components/justep/panel/panel');
var __parent1=require('$model/UI2/system/lib/base/modelBase'); 
var __parent0=require('$model/UI2/app/temperatures'); 
var __result = __parent1._extend(__parent0).extend({
	constructor:function(contextUrl){
	this.__sysParam='true';
	this.__contextUrl=contextUrl;
	this.__id='';
	this.__cid='cqqmEju';
	this._flag_='a05d9fe90d4602e4ea27768f9502c1ca';
	this._wCfg_={};
	this._appCfg_={};
	this.callParent(contextUrl);
 var __Data__ = require("$UI/system/components/justep/data/data");new __Data__(this,{"autoLoad":true,"confirmDelete":true,"confirmRefresh":true,"defCols":{"F_MeterName":{"define":"F_MeterName","label":"F_MeterName","name":"F_MeterName","relation":"F_MeterName","type":"String"},"f_Value":{"define":"f_Value","label":"f_Value","name":"f_Value","relation":"f_Value","type":"String"},"humi":{"define":"humi","label":"humi","name":"humi","relation":"humi","type":"String"},"temp":{"define":"temp","label":"temp","name":"temp","relation":"temp","type":"String"}},"directDelete":false,"events":{},"idColumn":"f_Value","isMain":false,"limit":20,"xid":"subdata"});
}}); 
return __result;});