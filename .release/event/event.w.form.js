define(function(require){
require('$model/UI2/system/components/justep/model/model');
require('$model/UI2/system/components/justep/loadingBar/loadingBar');
require('$model/UI2/system/components/justep/button/button');
require('$model/UI2/system/components/justep/input/input');
require('$model/UI2/system/components/justep/scrollView/scrollView');
require('$model/UI2/system/components/justep/list/list');
require('$model/UI2/system/components/justep/panel/child');
require('$model/UI2/system/components/justep/windowDialog/windowDialog');
require('$model/UI2/system/components/justep/labelEdit/labelEdit');
require('$model/UI2/system/components/justep/wing/child');
require('$model/UI2/system/components/justep/panel/panel');
require('$model/UI2/system/components/bootstrap/table/table');
require('$model/UI2/system/components/justep/select/select');
require('$model/UI2/system/components/justep/output/output');
require('$model/UI2/system/components/justep/data/data');
require('$model/UI2/system/components/justep/window/window');
require('$model/UI2/system/components/justep/wing/wing');
var __parent1=require('$model/UI2/system/lib/base/modelBase'); 
var __parent0=require('$model/UI2/app/event'); 
var __result = __parent1._extend(__parent0).extend({
	constructor:function(contextUrl){
	this.__sysParam='true';
	this.__contextUrl=contextUrl;
	this.__id='';
	this.__cid='c7RFJjm';
	this._flag_='827701bfb5b12d8d1199637124b29346';
	this._wCfg_={};
	this._appCfg_={};
	this.callParent(contextUrl);
 var __Data__ = require("$UI/system/components/justep/data/data");new __Data__(this,{"autoLoad":true,"confirmDelete":true,"confirmRefresh":true,"defCols":{"f_SubID":{"define":"f_SubID","label":"","name":"f_SubID","relation":"f_SubID","type":"String"},"f_SubName":{"define":"f_SubName","label":"","name":"f_SubName","relation":"f_SubName","type":"String"}},"directDelete":false,"events":{},"idColumn":"f_SubID","initData":"[]","isMain":false,"limit":20,"xid":"subdata"});
 new __Data__(this,{"autoLoad":true,"confirmDelete":true,"confirmRefresh":true,"defCols":{"alarmdesc":{"define":"alarmdesc","name":"alarmdesc","relation":"alarmdesc","type":"String"},"alarmtype":{"define":"alarmtype","name":"alarmtype","relation":"alarmtype","type":"String"},"endtime":{"define":"endtime","name":"endtime","relation":"endtime","type":"String"},"limitvalue":{"define":"limitvalue","name":"limitvalue","relation":"limitvalue","type":"String"},"metername":{"define":"metername","name":"metername","relation":"metername","type":"String"},"paramname":{"define":"paramname","name":"paramname","relation":"paramname","type":"String"},"starttime":{"define":"starttime","name":"starttime","relation":"starttime","type":"String"},"value":{"define":"value","name":"value","relation":"value","type":"String"}},"directDelete":false,"events":{},"idColumn":"starttime","initData":"[]","isMain":false,"limit":20,"xid":"eventdata"});
 new __Data__(this,{"autoLoad":true,"confirmDelete":true,"confirmRefresh":true,"defCols":{"eventtype":{"define":"eventtype","name":"eventtype","relation":"eventtype","type":"String"},"typename":{"define":"typename","name":"typename","relation":"typename","type":"String"}},"directDelete":false,"events":{},"idColumn":"eventtype","initData":"[{\"eventtype\":\"1\",\"typename\":\"越限事件\"},{\"eventtype\":\"2\",\"typename\":\"遥信事件\"}]","isMain":false,"limit":20,"xid":"eventtypedata"});
 var justep = require('$UI/system/lib/justep');if(!this['__justep__']) this['__justep__'] = {};if(!this['__justep__'].selectOptionsAfterRender)	this['__justep__'].selectOptionsAfterRender = function($element) {		var comp = justep.Component.getComponent($element);		if(comp) comp._addDefaultOption();	};if(!this['__justep__'].selectOptionsBeforeRender)	this['__justep__'].selectOptionsBeforeRender = function($element) {		var comp = justep.Component.getComponent($element);		if(comp) comp._optionsBeforeRender();	};
 var justep = require('$UI/system/lib/justep');if(!this['__justep__']) this['__justep__'] = {};if(!this['__justep__'].selectOptionsAfterRender)	this['__justep__'].selectOptionsAfterRender = function($element) {		var comp = justep.Component.getComponent($element);		if(comp) comp._addDefaultOption();	};if(!this['__justep__'].selectOptionsBeforeRender)	this['__justep__'].selectOptionsBeforeRender = function($element) {		var comp = justep.Component.getComponent($element);		if(comp) comp._optionsBeforeRender();	};
}}); 
return __result;});