define(function(require){
require('$model/UI2/system/components/justep/model/model');
require('$model/UI2/system/components/justep/loadingBar/loadingBar');
require('$model/UI2/system/components/justep/button/button');
require('$model/UI2/system/components/bootstrap/table/table');
require('$model/UI2/system/components/justep/scrollView/scrollView');
require('$model/UI2/system/components/justep/list/list');
require('$model/UI2/system/components/justep/panel/child');
require('$model/UI2/system/components/justep/data/data');
require('$model/UI2/system/components/justep/windowDialog/windowDialog');
require('$model/UI2/system/components/justep/window/window');
require('$model/UI2/system/components/justep/panel/panel');
var __parent1=require('$model/UI2/system/lib/base/modelBase'); 
var __parent0=require('$model/UI2/app/assignment'); 
var __result = __parent1._extend(__parent0).extend({
	constructor:function(contextUrl){
	this.__sysParam='true';
	this.__contextUrl=contextUrl;
	this.__id='';
	this.__cid='ca6FjUf';
	this._flag_='90dcc837453e0ec11988efdfba7a7a2d';
	this._wCfg_={};
	this._appCfg_={};
	this.callParent(contextUrl);
 var __Data__ = require("$UI/system/components/justep/data/data");new __Data__(this,{"autoLoad":true,"confirmDelete":true,"confirmRefresh":true,"defCols":{"address":{"define":"address","label":"addRess","name":"address","relation":"address","type":"String"},"deadline":{"define":"deadline","label":"deadLine","name":"deadline","relation":"deadline","type":"String"},"groupid":{"define":"groupid","label":"group","name":"groupid","relation":"groupid","type":"String"},"planid":{"define":"planid","label":"planID","name":"planid","relation":"planid","type":"String"},"subname":{"define":"subname","label":"subName","name":"subname","relation":"subname","type":"String"}},"directDelete":false,"events":{},"idColumn":"planid","isMain":false,"limit":20,"xid":"assigndata"});
}}); 
return __result;});