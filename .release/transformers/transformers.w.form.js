define(function(require){
require('$model/UI2/system/components/justep/model/model');
require('$model/UI2/system/components/justep/loadingBar/loadingBar');
require('$model/UI2/system/components/justep/windowReceiver/windowReceiver');
require('$model/UI2/system/components/bootstrap/table/table');
require('$model/UI2/system/components/justep/scrollView/scrollView');
require('$model/UI2/system/components/justep/list/list');
require('$model/UI2/system/components/justep/panel/child');
require('$model/UI2/system/components/justep/data/data');
require('$model/UI2/system/components/justep/windowDialog/windowDialog');
require('$model/UI2/system/components/justep/window/window');
require('$model/UI2/system/components/justep/panel/panel');
var __parent1=require('$model/UI2/system/lib/base/modelBase'); 
var __parent0=require('$model/UI2/app/transformers'); 
var __result = __parent1._extend(__parent0).extend({
	constructor:function(contextUrl){
	this.__sysParam='true';
	this.__contextUrl=contextUrl;
	this.__id='';
	this.__cid='c6zaIFv';
	this._flag_='3fde7244b6c22a183cd9912a22c47f69';
	this._wCfg_={};
	this._appCfg_={};
	this.callParent(contextUrl);
 var __Data__ = require("$UI/system/components/justep/data/data");new __Data__(this,{"autoLoad":true,"confirmDelete":true,"confirmRefresh":true,"defCols":{"LFValue":{"define":"LFValue","label":"LFValue","name":"LFValue","relation":"LFValue","type":"String"},"PFValue":{"define":"PFValue","label":"PFValue","name":"PFValue","relation":"PFValue","type":"String"},"PValue":{"define":"PValue","label":"PValue","name":"PValue","relation":"PValue","type":"String"},"QValue":{"define":"QValue","label":"QValue","name":"QValue","relation":"QValue","type":"String"},"f_SubID":{"define":"f_SubID","label":"f_SubID","name":"f_SubID","relation":"f_SubID","type":"String"},"subName":{"define":"subName","label":"subName","name":"subName","relation":"subName","type":"String"}},"directDelete":false,"events":{},"idColumn":"f_SubID","isMain":false,"limit":20,"xid":"subdata"});
}}); 
return __result;});