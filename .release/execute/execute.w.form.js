define(function(require){
require('$model/UI2/system/components/justep/model/model');
require('$model/UI2/system/components/justep/loadingBar/loadingBar');
require('$model/UI2/system/components/justep/button/button');
require('$model/UI2/system/components/justep/data/data');
require('$model/UI2/system/components/justep/window/window');
require('$model/UI2/system/components/justep/textarea/textarea');
require('$model/UI2/system/components/justep/messageDialog/messageDialog');
var __parent1=require('$model/UI2/system/lib/base/modelBase'); 
var __parent0=require('$model/UI2/app/execute'); 
var __result = __parent1._extend(__parent0).extend({
	constructor:function(contextUrl){
	this.__sysParam='true';
	this.__contextUrl=contextUrl;
	this.__id='';
	this.__cid='c2EZfUf';
	this._flag_='bf23ada33c3a247bf1e149a813230783';
	this.callParent(contextUrl);
 var __Data__ = require("$UI/system/components/justep/data/data");new __Data__(this,{"autoLoad":true,"confirmDelete":true,"confirmRefresh":true,"defCols":{"address":{"define":"address","label":"地点","name":"address","relation":"address","type":"String"},"completetime":{"define":"completetime","label":"计划完成日期","name":"completetime","relation":"completetime","rules":{"date":true},"type":"Date"},"endtime":{"define":"endtime","label":"执行结束时间","name":"endtime","relation":"endtime","rules":{"datetime":true},"type":"DateTime"},"planid":{"define":"planid","label":"计划id","name":"planid","relation":"planid","type":"String"},"result":{"define":"result","label":"巡检记录","name":"result","relation":"result","type":"String"},"starttime":{"define":"starttime","label":"执行开始时间","name":"starttime","relation":"starttime","type":"String"},"subname":{"define":"subname","label":"变电站名称","name":"subname","relation":"subname","type":"String"}},"directDelete":false,"events":{},"idColumn":"planid","initData":"[{}]","limit":20,"xid":"excutedata"});
 new __Data__(this,{"autoLoad":true,"confirmDelete":true,"confirmRefresh":true,"defCols":{"over":{"define":"over","name":"over","relation":"over","type":"Boolean"},"result":{"define":"result","name":"result","relation":"result","type":"Boolean"},"start":{"define":"start","name":"start","relation":"start","type":"Boolean"}},"directDelete":false,"events":{},"idColumn":"start","limit":20,"xid":"buttondata"});
}}); 
return __result;});