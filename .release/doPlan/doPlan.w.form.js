define(function(require){
require('$model/UI2/system/components/justep/model/model');
require('$model/UI2/system/components/justep/loadingBar/loadingBar');
require('$model/UI2/system/components/justep/button/button');
require('$model/UI2/system/components/bootstrap/table/table');
require('$model/UI2/system/components/justep/scrollView/scrollView');
require('$model/UI2/system/components/justep/list/list');
require('$model/UI2/system/components/justep/panel/child');
require('$model/UI2/system/components/justep/data/data');
require('$model/UI2/system/components/justep/window/window');
require('$model/UI2/system/components/justep/panel/panel');
var __parent1=require('$model/UI2/system/lib/base/modelBase'); 
var __parent0=require('$model/UI2/app/doPlan'); 
var __result = __parent1._extend(__parent0).extend({
	constructor:function(contextUrl){
	this.__sysParam='true';
	this.__contextUrl=contextUrl;
	this.__id='';
	this.__cid='cuiyyqe';
	this._flag_='6166108d641a04098638840d76ca5bdc';
	this._wCfg_={};
	this._appCfg_={};
	this.callParent(contextUrl);
 var __Data__ = require("$UI/system/components/justep/data/data");new __Data__(this,{"autoLoad":true,"confirmDelete":true,"confirmRefresh":true,"defCols":{"address":{"define":"address","label":"地点","name":"address","relation":"address","type":"String"},"completetime":{"define":"completetime","label":"计划完成日期","name":"completetime","relation":"completetime","rules":{"date":true},"type":"Date"},"planid":{"define":"planid","label":"计划id","name":"planid","relation":"planid","type":"String"},"subname":{"define":"subname","label":"变配电站","name":"subname","relation":"subname","type":"String"}},"directDelete":false,"events":{},"idColumn":"planid","isMain":false,"limit":20,"xid":"plandata"});
}}); 
return __result;});