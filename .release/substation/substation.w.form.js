define(function(require){
require('$model/UI2/system/components/justep/model/model');
require('$model/UI2/system/components/justep/loadingBar/loadingBar');
require('$model/UI2/system/components/justep/button/button');
require('$model/UI2/system/components/bootstrap/table/table');
require('$model/UI2/system/components/justep/scrollView/scrollView');
require('$model/UI2/system/components/justep/list/list');
require('$model/UI2/system/components/justep/panel/child');
require('$model/UI2/system/components/justep/data/data');
require('$model/UI2/system/components/justep/windowDialog/windowDialog');
require('$model/UI2/system/components/justep/window/window');
require('$model/UI2/system/components/justep/panel/panel');
var __parent1=require('$model/UI2/system/lib/base/modelBase'); 
var __parent0=require('$model/UI2/app/substation'); 
var __result = __parent1._extend(__parent0).extend({
	constructor:function(contextUrl){
	this.__sysParam='true';
	this.__contextUrl=contextUrl;
	this.__id='';
	this.__cid='cfaQnUr';
	this._flag_='704f5ba3f8227034d449f093e290a6d2';
	this._wCfg_={};
	this._appCfg_={};
	this.callParent(contextUrl);
 var __Data__ = require("$UI/system/components/justep/data/data");new __Data__(this,{"autoLoad":true,"confirmDelete":true,"confirmRefresh":true,"defCols":{"f_Address":{"define":"f_Address","label":"f_Address","name":"f_Address","relation":"f_Address","type":"String"},"f_SubID":{"define":"f_SubID","label":"f_SubID","name":"f_SubID","relation":"f_SubID","type":"String"},"f_SubName":{"define":"f_SubName","label":"f_SubName","name":"f_SubName","relation":"f_SubName","type":"String"}},"directDelete":false,"events":{},"idColumn":"f_SubID","isMain":false,"limit":20,"xid":"subdata"});
}}); 
return __result;});