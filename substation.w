﻿<?xml version="1.0" encoding="utf-8"?>

<div xmlns="http://www.w3.org/1999/xhtml" xid="window" class="window" component="$UI/system/components/justep/window/window"
  design="device:m;">  
  <div component="$UI/system/components/justep/model/model" xid="model" style="height:auto;top:241px;left:606px;" onLoad="modelParamsReceive"><div component="$UI/system/components/justep/data/data" autoLoad="true" xid="subdata" idColumn="f_SubID"><column label="f_SubID" name="f_SubID" type="String" xid="xid1"></column>
  <column label="f_SubName" name="f_SubName" type="String" xid="xid2"></column>
  <column label="f_Address" name="f_Address" type="String" xid="xid3"></column></div>
  </div>  
  <html lang="zh-CN"> 
    <head> 
      <meta charset="utf-8"/>  
      <meta http-equiv="X-UA-Compatible" content="IE=edge"/>  
      <meta name="viewport" content="width=device-width, initial-scale=1"/>  
      <title>变电所运维云平台</title>  
      <link href="css/bootstrap.min.css" rel="stylesheet"/>  
      <link href="css/main.css" rel="stylesheet"/> 
    </head>  
   
  </html>  
  <span component="$UI/system/components/justep/windowDialog/windowDialog" xid="windowDialog1"
    src="$UI/app/distribute.w" status="normal" width="80%" height="60%" style="top:228px;left:281px;"/> 
<div component="$UI/system/components/justep/panel/panel" class="x-panel x-full" xid="panel1">
   <div class="x-panel-top" xid="top1" height="43"> <nav class="nav center"> 
          <i class="left" bind-click="{operation:'window.close'}"/>  
          <span><![CDATA[变配电站列表]]></span>  
          <a href="#"> 
           <!--  <img src="image/menu.png" class="menu"/> -->
          </a>  
          <div class="menu-list"> 
            <ul> 
              <li> 
                <a href="#">地图导航</a> 
              </li>  
              <li> 
                <a href="#">变配电站概况</a> 
              </li>  
              <li class="inspect"> 
                <h2> 
                  <a href="#">巡检管理</a>  
                  <i class="foldIcon"/> 
                </h2>  
                <ul class="menu-child"> 
                  <li> 
                    <a href="#">巡检任务</a> 
                  </li>  
                  <li> 
                    <a href="#">任务执行</a> 
                  </li>  
                  <li> 
                    <a href="#">查询任务</a> 
                  </li> 
                </ul> 
              </li>  
              <li> 
                <a href="#">事件</a> 
              </li> 
            </ul> 
          </div> 
        </nav>  </div>
   <div class="x-panel-content  x-scroll-view" xid="content1" _xid="C794C5517C800001E4981750113B3090"><div class="x-scroll" component="$UI/system/components/justep/scrollView/scrollView" xid="scrollView1" autoAppend="true">
   <div class="x-content-center x-pull-down container" xid="div1">
    <i class="x-pull-down-img glyphicon x-icon-pull-down" xid="i1"></i>
    <span class="x-pull-down-label" xid="span1">下拉刷新...</span></div> 
   <div class="x-scroll-content" xid="div2"><div component="$UI/system/components/justep/list/list" class="x-list" xid="list4" data="subdata" limit="10"> 
          <table class="table table-bordered table-hover table-striped" component="$UI/system/components/bootstrap/table/table" xid="table2"> 
            <thead xid="thead4"> 
              <tr xid="tr6"> 
                <th xid="col11" style="width:166px;height:40px;text-align:center;background-color:#3DB3FF;color:#FFFFFF;">名称</th>  
                
                <th xid="col12" style="width:165px;text-align:center;background-color:#3DB3FF;color:#FFFFFF;"><![CDATA[地点]]></th>  
                <th xid="col13" style="width:165px;text-align:center;background-color:#3DB3FF;color:#FFFFFF;">详情</th> 
              </tr> 
            </thead>  
            <tbody class="x-list-template" xid="listTemplate3"> 
              <tr xid="tr7"> 
                <td xid="td7" bind-text='ref("f_SubName")' />  
                
                <td xid="td8" bind-text='ref("f_Address")' />  
                <td xid="td9" style="text-align:center;">
                  <a component="$UI/system/components/justep/button/button" class="btn btn-info" label="详情" xid="button3" onClick="button3Click" style="background-color:#FF6C1F;"> 
                    <i xid="i3" />  
                    <span xid="span8">详情</span>
                  </a>
                </td> 
              </tr> 
            </tbody> 
          </table> 
        </div></div>
   <div class="x-content-center x-pull-up" xid="div3">
    </div> </div></div>
   </div></div>
