<?xml version="1.0" encoding="utf-8"?>

<div xmlns="http://www.w3.org/1999/xhtml" xid="window" class="window" component="$UI/system/components/justep/window/window"
  design="device:m;">  
  <div component="$UI/system/components/justep/model/model" xid="model" style="top:250px;left:668px;height:auto;" onLoad="modelLoad">
  <div component="$UI/system/components/justep/data/data" autoLoad="true" xid="noreadeventdata" idColumn="subname">
  <column name="subname" type="String" xid="xid1"></column>
  <column name="starttime" type="String" xid="xid2"></column>
  <column name="metername" type="String" xid="xid3"></column>
  <column name="alarmtype" type="String" xid="xid4"></column>
  <column name="alarmdesc" type="String" xid="xid5"></column></div></div>  
  <html lang="zh-CN"> 
    <head> 
      <meta charset="utf-8"/>  
      <meta http-equiv="X-UA-Compatible" content="IE=edge"/>  
      <meta name="viewport" content="width=device-width, initial-scale=1"/>  
      <title>变电所运维云平台</title>  
      <link href="css/bootstrap.min.css" rel="stylesheet"/>  
      <link href="css/main.css" rel="stylesheet"/> 
    </head>  
    </html>  
  <div component="$UI/system/components/justep/panel/panel" class="x-panel x-full" xid="panel1">
   <div class="x-panel-top" xid="top1"><nav class="nav center"> 
          <span><![CDATA[报警事件]]></span>  
          <a href="#"> 
            <!-- <img src="image/menu.png" class="menu"/>  -->
          </a>  
          <div class="menu-list"> 
            <ul> 
              <li> 
                <a href="#">地图导航</a> 
              </li>  
              <li> 
                <a href="#">变电所概况</a> 
              </li>  
              <li class="inspect"> 
                <h2> 
                  <a href="#">巡检管理</a>  
                  <i class="foldIcon"/> 
                </h2>  
                <ul class="menu-child"> 
                  <li> 
                    <a href="#">巡检任务</a> 
                  </li>  
                  <li> 
                    <a href="#">任务执行</a> 
                  </li>  
                  <li> 
                    <a href="#">查询任务</a> 
                  </li> 
                </ul> 
              </li>  
              <li> 
                <a href="#">事件</a> 
              </li> 
            </ul> 
          </div> 
        </nav>  </div>
   <div class="x-panel-content  x-scroll-view" xid="content1" _xid="C79983D2C3900001C22AB43F87701E7D"><div class="x-scroll" component="$UI/system/components/justep/scrollView/scrollView" xid="scrollView1">
   <div class="x-content-center x-pull-down container" xid="div1">
    <i class="x-pull-down-img glyphicon x-icon-pull-down" xid="i1"></i>
    <span class="x-pull-down-label" xid="span1">下拉刷新...</span></div> 
   <div class="x-scroll-content" xid="div2"><div component="$UI/system/components/justep/list/list" class="x-list" xid="list4" data="noreadeventdata" limit="6"> 
          <table class="table table-bordered table-hover table-striped" component="$UI/system/components/bootstrap/table/table" xid="table2"> 
            <thead xid="thead4"> 
              <tr xid="tr6"> 
                <th xid="col1" style="text-align:center;background-color:#3DB3FF;color:#FFFFFF;"><![CDATA[变配电站]]></th>
                <th xid="col11" style="text-align:center;background-color:#3DB3FF;color:#FFFFFF;">时间</th>  
                <th xid="col14" style="text-align:center;background-color:#3DB3FF;color:#FFFFFF;">类型</th>  
                <th xid="col12" style="text-align:center;background-color:#3DB3FF;color:#FFFFFF;">回路名称</th>  
                <th xid="col13" style="text-align:center;background-color:#3DB3FF;color:#FFFFFF;">描述</th> 
              </tr> 
            </thead>  
            <tbody class="x-list-template" xid="listTemplate3"> 
              <tr xid="tr7"> 
                <td xid="td1" bind-text='ref("subname")'></td>
                <td xid="td7" bind-text='ref("starttime")' />  
                <td xid="td10" bind-text='ref("alarmtype")' />  
                <td xid="td8" bind-text='ref("metername")' />  
                <td xid="td9" style="text-align:center;" bind-text='ref("alarmdesc")'>
                  </td> 
              </tr> 
            </tbody> 
          </table> 
        </div></div>
   <div class="x-content-center x-pull-up" xid="div3">
    </div> </div></div>
   <div class="x-panel-bottom" xid="bottom1"><nav class="nav-bottom"> 
          <ul> 
            <li bind-click="li1Click"> 
              <span class="home2"/>  
              <p>主页</p> 
            </li>  
            <li> 
              <span class="alarm2" ></span><p>报警信息</p> 
            </li>  
            <li bind-click="li2Click"> 
              <span class="contact"/>  
              <p>关于</p> 
            </li>
          </ul> 
        </nav>  </div></div></div>
