<?xml version="1.0" encoding="utf-8"?>

<div xmlns="http://www.w3.org/1999/xhtml" xid="window" class="window" component="$UI/system/components/justep/window/window"
  design="device:m;">  
  <div component="$UI/system/components/justep/model/model" xid="model" style="top:428px;left:332px;height:auto;">
    <div component="$UI/system/components/justep/data/data" autoLoad="true"
      xid="subdata" idColumn="f_CollectTime">
      <column label="f_CollectTime" name="f_CollectTime" type="String" xid="xid1"/>  
      <column label="f_TempA" name="f_TempA" type="String" xid="xid2"/>  
      <column label="f_TempB" name="f_TempB" type="String" xid="xid3"/>  
      <column label="f_TempC" name="f_TempC" type="String" xid="xid4"/>  
      <column label="f_MeterName" name="f_MeterName" type="String" xid="xid5"/>
    </div>
  </div>  
  <div>
    <div xid="div3" class="meterName"><span xid="span3"><![CDATA[仪表名称： ]]></span><span xid="span2" id="span2"></span></div><input component="$UI/system/components/justep/input/input" class="form-control"
      xid="startdate" style="width:273px;" dataType="Date" disabled="true"/>  
    <button xid="button1" bind-click="button1Click"><![CDATA[上一日]]></button>
    <button xid="button2" bind-click="button2Click"><![CDATA[下一日]]></button>
    <button xid="button3" style="width:51px;" bind-click="button3Click" id="button3"><![CDATA[图表]]></button>
    <button xid="button4" style="width:56px;" bind-click="button4Click" id="button4"><![CDATA[数据]]></button>
    <div xid="domEcharts" id="domEcharts" align="left" style="width:100%;height:300px;"/>  
    <div xid="domTable" id="domTable">
      <div component="$UI/system/components/fragment/list/listTable" xid="listTable1"> 
        <div component="$UI/system/components/justep/list/list" class="x-list"
          xid="list1" data="subdata"> 
          <table class="table table-bordered table-hover table-striped" component="$UI/system/components/bootstrap/table/table"
            xid="table1" style="height:400px;"> 
            <thead xid="thead1"> 
              <tr xid="tr1"> 
                <th xid="col3"><![CDATA[采集时间]]></th>  
                <th xid="col4"><![CDATA[温度A]]></th>  
                <th xid="col5"><![CDATA[温度B]]></th>  
                <th xid="col6"><![CDATA[温度C]]></th>
              </tr> 
            </thead>  
            <tbody class="x-list-template" xid="listTemplate1"> 
              <tr xid="tr2"> 
                <td xid="td3" bind-text='ref("f_CollectTime")'/>  
                <td xid="td5" bind-text="ref(&quot;f_TempA&quot;)"/>  
                <td xid="td6" bind-text="ref(&quot;f_TempB&quot;)"/>
                <td xid="td4" bind-text="ref(&quot;f_TempC&quot;)"/>
              </tr> 
            </tbody> 
          </table> 
        </div> 
      </div>
    </div> 
  </div>  
  <span component="$UI/system/components/justep/windowReceiver/windowReceiver"
    xid="windowReceiver" onReceive="windowReceiver"/>
</div>
