define(function(require) {
	var $ = require("jquery");
	var justep = require("$UI/system/lib/justep");

	var Model = function() {
		this.callParent();
	};

	var subdata;
	var userid;
	var ipAdress;
	
	Model.prototype.li1Click = function(event) {
		justep.Shell.closeAllOpendedPages();
	};

	Model.prototype.li2Click = function(event){
		justep.Shell.showPage("alarm");
	};
	
	Model.prototype.windowReceiver1Receive = function(event){
		userid = JSON.parse(localStorage.getItem("userUUID")).userid;
		subdata = this.comp("subdata");
		ipAdress = localStorage.getItem("ipAdress");
		subid = event.data;
		var url = "http://" + ipAdress + "/SubstationOperation/rest/app/getTempHumi";
		$.ajax({
			type : "get",
			async : false,
			data : "fSubid=" + subid,
			url : url,
			cache : false,
			dataType : "jsonp",
			jsonp : "Callback",
			jsonpCallback : "successCallback",
			success : function(success) {
			debugger;
				subdata.clear();
				if (success != null)
					subdata.loadData(success);
			}
		});
	};


	return Model;
});