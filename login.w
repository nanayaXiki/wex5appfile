<?xml version="1.0" encoding="utf-8"?>

<div xmlns="http://www.w3.org/1999/xhtml" xid="window" class="window" component="$UI/system/components/justep/window/window"
  design="device:m;">  
  <div component="$UI/system/components/justep/model/model" xid="model" style="top:246px;left:621px;height:auto;" onLoad="modelLoad"> 
    <div component="$UI/system/components/justep/data/data" autoLoad="true"
      xid="userdata" idColumn="userid"> 
      <column label="userid" name="userid" type="String" xid="xid1"/>  
      <column label="username" name="username" type="String" xid="xid2"/> 
    </div> 
  </div>  
  <html lang="zh-CN"> 
    <head> 
      <meta charset="utf-8"/>  
      <meta http-equiv="X-UA-Compatible" content="IE=edge"/>  
      <meta name="viewport" content="width=device-width, initial-scale=1"/>  
      <title>变电所运维云平台</title>  
      <link href="css/bootstrap.min.css" rel="stylesheet"/>  
      <link href="css/main.css" rel="stylesheet"/> 
    </head>  
    <body class="login-bg"> 
      <div class="content-fluid login-fluid"> 
        <div class="col-xs-12 login-nav"> 
          <img src="image/login-p.png"/>  
          <h2><![CDATA[电力运维云平台]]></h2> 
        </div>
        <div class="login-name"> 
          <span class="login-icon3"/>  
          <input type="text" placeholder="域名/服务器地址加端口号" component="$UI/system/components/justep/input/input"
            xid="ipAdress"/> 
        </div>  
        <div class="login-name"> 
          <span class="login-icon"/>  
          <input type="text" placeholder="用户名" component="$UI/system/components/justep/input/input"
            xid="username"/> 
        </div>  
        <div class="login-name login-bottom"> 
          <span class="login-icon2"/>  
          <input type="password" placeholder="密码" component="$UI/system/components/justep/input/password"
            xid="password"/> 
        </div>  
        <button type="button" component="$UI/system/components/justep/button/button"
          xid="loginBtn" onClick="loginBtnClick">登 录</button> 
      </div> 
    </body> 
  </html>  
  <span component="$UI/system/components/justep/windowReceiver/windowReceiver"
    xid="windowReceiver1" style="top:320px;left:104px;"/> 
</div>
