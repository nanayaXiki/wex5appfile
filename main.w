<?xml version="1.0" encoding="utf-8"?>

<div xmlns="http://www.w3.org/1999/xhtml" xid="window" class="window container-fluid" component="$UI/system/components/justep/window/window"
  design="device:m;">  
  <html lang="zh-CN"> 
      <meta charset="utf-8" />  
      <meta http-equiv="X-UA-Compatible" content="IE=edge" />  
      <meta name="viewport" content="width=device-width, initial-scale=1" />  
      <title>变电所运维云平台</title>  
      <link href="css/bootstrap.min.css" rel="stylesheet" />  
      <link href="css/main.css" rel="stylesheet" />
      <script type="text/javascript">
    	// JavaScript Document
			(function flexible (window, document) {
			  var docEl = document.documentElement;     //获取文档根节点并保存到变量docEl中(相当于获取到html对象)
			  var dpr = window.devicePixelRatio || 1;   //获取像素比，如果window.devicePixelRatio为false是dpr为1，如果window.devicePixelRatio大于1，那么dpr赋值取大的数

			  function setBodyFontSize () {
			    if (document.body) { //获取到body对象是否存在，个人觉得啰嗦
			      document.body.style.fontSize = (12 * dpr) + 'px';
			    }   
			    else {
			      document.addEventListener('DOMContentLoaded', setBodyFontSize);
			    }
			  }
			  setBodyFontSize();

			  // set 1rem = viewWidth / 10
			  function setRemUnit () {
			    var rem = docEl.clientWidth / 10
			    docEl.style.fontSize = rem + 'px'
			  }

			  setRemUnit()

			  // reset rem unit on page resize
			  window.addEventListener('resize', setRemUnit)
			  window.addEventListener('pageshow', function (e) {
			    if (e.persisted) {
			      setRemUnit()
			    }
			  })

			  // detect 0.5px supports
			  if (dpr >= 2) {
			    var fakeBody = document.createElement('body')
			    var testElement = document.createElement('div')
			    testElement.style.border = '.5px solid transparent'
			    fakeBody.appendChild(testElement)
			    docEl.appendChild(fakeBody)
			    if (testElement.offsetHeight === 1) {
			      docEl.classList.add('hairlines')
			    }
			    docEl.removeChild(fakeBody)
			  }
			}(window, document))
    </script>
  
    </html><div component="$UI/system/components/justep/model/model" xid="model" style="top:170px;left:625px;height:auto;" onLoad="modelLoad" onActive="modelActive" onInactive="modelInactive">
    </div>  
  <span component="$UI/system/components/justep/messageDialog/messageDialog" xid="logout" type="OKCancel" message="确定要退出登录吗？" onCancel="logoutOK" style="top:310px;left:139px;"></span>
  <span component="$UI/system/components/justep/windowDialog/windowDialog" xid="loginDialog" src="$UI/app/login.w" forceRefreshOnOpen="true" style="top:345px;left:364px;"></span>
  <div xid="div1" bind-click="div1Click"><span component="$UI/system/components/justep/windowDialog/windowDialog" xid="windowDialog1" src="$UI/app/inspect.w" width="auto" showTitle="false" status="normal" height="auto" routable="true" style="top:339px;left:433px;"/></div>
  <div component="$UI/system/components/justep/panel/panel" class="x-panel x-full x-has-iosstatusbar" xid="panel2">
   <div class="x-panel-top" xid="top1" height="43"><nav class="nav" style="text-align:center"> 
            <span><![CDATA[电力运维云平台]]></span>  
            <a href="#"> 
              <img src="image/menu.png" class="menu" xid="menu"/> 
            </a>  
            <div class="menu-list"> 
              <ul> 
                <li id="logout"> 
                  <a href="#" bind-click="logoutClick">退出登录</a> 
                </li> 
              </ul> 
            </div> 
          </nav>  </div>
   <div class="x-panel-content " xid="content1"><div> 
          
          <img src="image/banner.png" class="banner" />  
          <ul class="main-menu"> 
            <li> 
              <span class="map" xid="map" bind-click="mapClick" />  
              <p>地图导航</p> 
            </li>  
            <li> 
              <span class="survey" bind-click="span2Click" />  
              <p>变配电站概况</p> 
            </li>  
            <li xid="myModal2" bind-click="myModal2Click"> 
              <span class="manage" />  
              <p>巡检管理</p> 
            </li>  
            <li> 
              <span class="event" bind-click="span3Click" />  
              <p>事件</p> 
            </li> 
            <li> 
              <span class="elec" bind-click="span1Click" />  
              <p>电力参数</p> 
            </li> 
         <li>
         <span xid="span1" class="safe" bind-click="safespanClick"></span><p><![CDATA[线缆温度]]></p>
         </li>
         <li xid="li2"><span xid="video" bind-click="videoClick" class="videoli"></span>
  <p xid="p2"><![CDATA[视频直播]]></p></li></ul>  
          
        </div></div>
   <div class="x-panel-bottom" xid="bottom1" height="67"><nav class="nav-bottom"> 
            <ul> 
              <li> 
                <span class="index"/>  
                <p>主页</p> 
              </li>  
              <li bind-click="li1Click"> 
                <span class="alarm">
                <p id="eventnum" xid="eventnum" style="border-radius:40%;color:#FFFFFF;background-color:#FF0000;position:absolute;left:55%;"><![CDATA[]]></p>
  			</span>  
                <p>报警信息</p> 
              </li>  
              <li bind-click="li2Click"> 
                <span class="contact"/>  
                <p><![CDATA[关于]]></p> 
              </li>
            </ul> 
          </nav></div></div></div>
