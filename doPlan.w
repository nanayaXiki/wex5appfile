<?xml version="1.0" encoding="utf-8"?>

<div xmlns="http://www.w3.org/1999/xhtml" xid="window" class="window" component="$UI/system/components/justep/window/window"
  design="device:m;">  
  <div component="$UI/system/components/justep/model/model" xid="model" style="height:auto;top:241px;left:606px;" onActive="modelActive" onLoad="modelParamsReceive">
  <div component="$UI/system/components/justep/data/data" autoLoad="true" xid="plandata" idColumn="planid">
   <column label="计划id" name="planid" type="String" xid="xid1"></column>
   <column label="计划完成日期" name="completetime" type="Date" xid="xid2"></column>
   <column label="地点" name="address" type="String" xid="xid3"></column>
   <column label="变配电站" name="subname" type="String" xid="xid5"></column></div></div>  
  <html lang="zh-CN"> 
    <head> 
      <meta charset="utf-8"/>  
      <meta http-equiv="X-UA-Compatible" content="IE=edge"/>  
      <meta name="viewport" content="width=device-width, initial-scale=1"/>  
      <title>变电所运维云平台</title>  
      <link href="css/bootstrap.min.css" rel="stylesheet"/>  
      <link href="css/main.css" rel="stylesheet"/> 
    </head>  
    <body> 
        
     
    </body> 
  </html>  
  <div component="$UI/system/components/justep/panel/panel" class="x-panel x-full" xid="panel1">
   <div class="x-panel-top" xid="top1" height="43"> <nav class="nav center"> 
          <i class="left" bind-click="{operation:'window.close'}"/>  
          <span>任务列表</span>  
          <a href="#"> 
            <!-- <img src="image/menu.png" class="menu"/>  -->
          </a>  
          <div class="menu-list"> 
            <ul> 
              <li> 
                <a href="#">地图导航</a> 
              </li>  
              <li> 
                <a href="#">变配电站概况</a> 
              </li>  
              <li class="inspect"> 
                <h2> 
                  <a href="#">巡检管理</a>  
                  <i class="foldIcon"/> 
                </h2>  
                <ul class="menu-child"> 
                  <li> 
                    <a href="#">巡检任务</a> 
                  </li>  
                  <li> 
                    <a href="#">任务执行</a> 
                  </li>  
                  <li> 
                    <a href="#">查询任务</a> 
                  </li> 
                </ul> 
              </li>  
              <li> 
                <a href="#">事件</a> 
              </li> 
            </ul> 
          </div> 
        </nav>  </div>
   <div class="x-panel-content  x-scroll-view" xid="content1" _xid="C794C3480A0000017421A3E4118B5B00" style="top: 43px; bottom: 67px;">
  <div class="x-scroll" component="$UI/system/components/justep/scrollView/scrollView" xid="scrollView1" onPullDown="modelActive">
   <div class="x-content-center x-pull-down container" xid="div1">
    <i class="x-pull-down-img glyphicon x-icon-pull-down" xid="i1"></i>
    <span class="x-pull-down-label" xid="span1">下拉刷新...</span></div> 
   <div class="x-scroll-content" xid="div2"><div component="$UI/system/components/justep/list/list" class="x-list" xid="list4" data="plandata" limit="10"> 
          <table class="table table-bordered table-hover table-striped" component="$UI/system/components/bootstrap/table/table" xid="table2"> 
            <thead xid="thead4"> 
              <tr xid="tr6"> 
                <th xid="col11" style="width:166px;height:40px;text-align:center;background-color:#3DB3FF;color:#FFFFFF;" bind-text='$model.plandata.label("subname")'>名称</th>  
                <th xid="col14" style="text-align:center;background-color:#3DB3FF;color:#FFFFFF;width:200px;" bind-text='$model.plandata.label("completetime")'>计划完成日期</th>  
                <th xid="col12" style="width:165px;text-align:center;background-color:#3DB3FF;color:#FFFFFF;" bind-text='$model.plandata.label("address")'>地点</th>  
                <th xid="col13" style="width:165px;text-align:center;background-color:#3DB3FF;"><![CDATA[]]></th> 
              </tr> 
            </thead>  
            <tbody class="x-list-template" xid="listTemplate3"> 
              <tr xid="tr7"> 
                <td xid="td7" bind-text='ref("subname")' />  
                <td xid="td10" bind-text='ref("completetime")' style="width:100px;"/>  
                <td xid="td8" bind-text='ref("address")' />  
                <td xid="td9" style="text-align:center;">
                  <a component="$UI/system/components/justep/button/button" class="btn btn-info" label="执行" xid="button3" onClick="button3Click"> 
                    <i xid="i3" />  
                    <span xid="span8">执行</span>
                  </a>
                </td> 
              </tr> 
            </tbody> 
          </table> 
        </div></div>
   <div class="x-content-center x-pull-up" xid="div3">
    </div> </div></div>
   </div></div>
