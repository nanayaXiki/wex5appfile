<?xml version="1.0" encoding="utf-8"?>

<div xmlns="http://www.w3.org/1999/xhtml" xid="window" class="window container-fluid" component="$UI/system/components/justep/window/window"
  design="device:m;">  
  <html lang="zh-CN"> 
    <meta charset="utf-8"/>  
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>  
    <meta name="viewport" content="width=device-width, initial-scale=1"/>  
    <title>变电所运维云平台</title>  
    <link href="css/bootstrap.min.css" rel="stylesheet"/>  
    <link href="css/main.css" rel="stylesheet"/> 
  </html>  
  <div component="$UI/system/components/justep/model/model" xid="model" style="top:170px;left:625px;height:auto;"/>  
  <div xid="div1" bind-click="div1Click"></div>  
  <div component="$UI/system/components/justep/panel/panel" class="x-panel x-full"
    xid="panel2"> 
    <div class="x-panel-top" xid="top1" height="43" style="text-align:center;"> 
      <nav class="nav"> 
        <span style="text-align:center"><![CDATA[关于App]]></span>  
        <a href="#"></a>  
        <div class="menu-list"> 
          <ul> 
            <li id="logout"> 
              <a href="#" bind-click="logoutClick">退出登录</a> 
            </li> 
          </ul> 
        </div> 
      </nav> 
    </div>  
    <div style="background-color:#C0C0C0;height:100%;width:100%">
    	
    <img src="$UI/app/image/app-vis.png" alt="" xid="image2" align="middle" style="height:80%;width:100%"></img>
  <span component="$UI/system/components/justep/input/input" class="form-control" xid="input1" dataType="Integer" style="text-align:center;background-color:#FFFFFF;height:55px;font-size:large;font-weight:600;">
  版本号：v2.3.0</span></div>
    <!-- <div class="x-panel-content " xid="content1"> 
      <div> 
        <img src="image/us-banner.jpg" class="banner"/> 
      </div>  
      <div xid="div7" style="margin-top:10px;margin-bottom:10px;font-size:28px;">
        <div xid="div8" style="padding-right:3px;padding-left:3px;">
          <span xid="span9" style="font-size:20px;font-weight:700;"><![CDATA[安科瑞电气股份有限公司]]></span>
        </div>  
        <div xid="div9" style="margin:10px 10px 10px 10px;padding-right:3px;padding-left:3px;">
          <span xid="span10" style="font-size:large;font-weight:700;"><![CDATA[ACREL CO.,LTD]]></span>
        </div>
      </div>
      <div xid="div2" style="padding-right:10px;padding-left:10px;margin-top:10px;">
        <img src="$UI/app/image/con-address.png" alt="" xid="image1"/>  
        <span xid="span1" style="font-weight:300;font-size:large;margin-left:5px;"><![CDATA[地址:]]></span>  
        <span xid="span2" style="font-weight:500;font-size:large;margin-left:5px;"><![CDATA[上海市嘉定区育绿路253号]]></span>
      </div>  
      <div xid="div3" style="padding-right:10px;padding-left:10px;margin-top:10px;">
        <img src="$UI/app/image/tel.png" alt="" xid="image2"/>  
        <span xid="span3" style="font-size:large;font-weight:300;margin-left:5px;"><![CDATA[电话:]]></span>  
        <span xid="span4" style="font-size:large;font-weight:500;margin-left:10px;"><![CDATA[18702128769]]></span>
      </div>  
      <div xid="div4" style="padding-right:10px;padding-left:10px;margin-top:10px;">
        <img src="$UI/app/image/mail.png" alt="" xid="image3"/>  
        <span xid="span5" style="font-size:large;font-weight:300;margin-left:5px;"><![CDATA[邮箱:]]></span>  
        <span xid="span6" style="font-size:large;font-weight:500;margin-left:10px;"><![CDATA[jsjyshaohua@163.com]]></span>
      </div>  
      <div xid="div5" style="padding-right:10px;padding-left:10px;margin-top:10px;">
        <img src="$UI/app/image/fax.png" alt="" xid="image4"/>
        <span xid="span7" style="font-size:large;font-weight:300;margin-left:5px;"><![CDATA[传真:]]></span>  
        <span xid="span8" style="font-size:large;font-weight:500;margin-left:10px;"><![CDATA[021-69153629]]></span>
      </div> 
    </div>  --> 
    <div class="x-panel-bottom" xid="bottom1" height="67"> 
      <nav class="nav-bottom"> 
        <ul> 
          <li> 
            <span class="home2" bind-click="span11Click"/>  
            <p>主页</p> 
          </li>  
          <li bind-click="li1Click"> 
            <span class="alarm"> 
              <p id="eventnum" xid="eventnum" style="border-radius:40%;color:#FFFFFF;background-color:#FF0000;position:absolute;left:55%;"><![CDATA[]]></p> 
            </span>  
            <p>报警信息</p> 
          </li>  
          <li> 
            <span class="contact1"/>  
            <p><![CDATA[关于]]></p> 
          </li> 
        </ul> 
      </nav> 
    </div> 
  </div> 
</div>
