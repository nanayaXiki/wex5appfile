<?xml version="1.0" encoding="UTF-8"?>

<div xmlns="http://www.w3.org/1999/xhtml" xid="window" class="window" component="$UI/system/components/justep/window/window" design="device:m;">  
  <div component="$UI/system/components/justep/model/model" xid="model" style="height:auto;top:233px;left:609px;" onParamsReceive="modelParamsReceive" onActive="modelActive"><div component="$UI/system/components/justep/data/data" autoLoad="true" xid="excutedata" idColumn="planid"><column label="计划id" name="planid" type="String" xid="xid1"></column>
  <column label="变配电站名称" name="subname" type="String" xid="xid1"></column>
  <column label="地点" name="address" type="String" xid="xid2"></column>
  <column label="执行开始时间" name="starttime" type="String" xid="xid4"></column>
  <column label="计划完成日期" name="completetime" type="Date" xid="xid7"></column>
  <column label="执行结束时间" name="endtime" type="DateTime" xid="xid5"></column>
  <column label="巡检记录" name="result" type="String" xid="xid6"></column>
  <data xid="default1">[{}]</data></div>
  <div component="$UI/system/components/justep/data/data" autoLoad="true" xid="buttondata" idColumn="start"><column name="start" type="Boolean" xid="xid3"></column>
  <column name="over" type="Boolean" xid="xid8"></column>
  <column name="result" type="Boolean" xid="xid9"></column></div></div> 

<html lang="zh-CN">
  <head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <title>变电所运维云平台</title>
    <link href="css/bootstrap.min.css" rel="stylesheet"/>
    <link href="css/main.css" rel="stylesheet"/>
  </head>
  <body>
    <div class="content-fluid">
      <nav class="nav center">
        <i class="left" bind-click="{operation:'window.close'}"></i>
        <span>巡检任务执行</span>
        <!-- <a href="#"><img src="image/menu.png" class="menu"/></a> -->
        <div class="menu-list">
          <ul>
            <li><a href="#">地图导航</a></li>
            <li><a href="#">变配电站概况</a></li>
            <li class="inspect">
              <h2>
                <a href="#">巡检管理</a>
                <i class="foldIcon"></i>
              </h2>
              <ul class="menu-child">
                <li><a href="#">巡检任务</a></li>
                <li><a href="#">任务执行</a></li>
                <li><a href="#">查询任务</a></li>
              </ul>
            </li>
            <li><a href="#">事件</a></li>
          </ul>
        </div>
      </nav>
      <div class="execute">
        <p>
  <span xid="span7" style="width:118px;">变配电站名称：
  </span>
  <span xid="subname"></span></p>
        <p>
  <span xid="span10" style="width:118px;">地点：</span>
  <span xid="address"></span></p>
        <p>
  <span xid="span9">计划完成日期：</span>
  <span xid="completetime"></span></p>
        <p>
  <span xid="span8">执行开始时间：</span>
  <span xid="starttime"><![CDATA[任务未开始]]></span></p>
        <a component="$UI/system/components/justep/button/button" class="btn btn-default start"  label="开始" style="width:100%;background-color:#FF6C1F;color:#FFFFFF;" xid="start" onClick="startClick">
   <i></i>
   <span>开始</span></a>
        <p>巡检记录：
  </p>
        
        <textarea component="$UI/system/components/justep/textarea/textarea" class="form-control" xid="result" style="height:87px;" disabled="true"></textarea><a component="$UI/system/components/justep/button/button" class="btn btn-default start" label="结束" style="width:100%;color:#FFFFFF;background-color:#b5b5b6;" xid="over" onClick="overClick" disabled="true">
   <i xid="i3"></i>
   <span xid="span3">结束</span></a>
  </div>
      </div>
  
  </body>    
</html>
<span component="$UI/system/components/justep/messageDialog/messageDialog" xid="startdialog" type="YesNo" message="确认要开始执行吗？点击是后请扫描nfc" onYes="startdialogNo" onNo="startdialogOK"></span>
  <span component="$UI/system/components/justep/messageDialog/messageDialog" xid="enddialog" message="确认完成巡检任务吗？" type="YesNo" onYes="enddialogNo" onNo="enddialogOK"></span></div>